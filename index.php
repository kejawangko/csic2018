<?php 
//routing
include 'includes/url.php';
  if(!isset($_GET['page'])){
    if(isset($_SESSION['user_fullname'])) include 'view/index.php';
    else if(isset($_SESSION['admin_fullname'])) include 'view/admin/home.php';
    else include 'view/index.php';
  }
   
  
  else{
    if(!isset($_SESSION['user_fullname']) && !isset($_SESSION['admin_fullname'] )){ //masukkin list yang bole diakses walaupun user ga login
      switch($_GET['page']){
        case 'index':
          include 'view/index.php';
          break;
        case 'register':
          if(!isset($_POST["submit"])) include 'view/register.php';
          else include 'controller/register.php';
          break;
        case 'login':
          if(!isset($_POST["submit"])) include 'view/login.php';
          else include 'controller/login.php';
          break;
        case 'festAdmin':
          if(!isset($_POST["submit"])) include 'view/admin/login.php';
          else include 'controller/admin/login.php';
          break;
        case 'csic':
          if(isset($_GET['id']) ){
            if($_GET['id'] == 'registration'){
              header("Location: ".BASE_URL."register");
            }
          }
          else include 'view/csic.php';
          break;
        case 'cp':
          if(isset($_GET['id']) ){
            if($_GET['id'] == 'registerscpc' || $_GET['id'] == 'registerjcpc'){
              header("Location: ".BASE_URL."register");
            }
          }
          break;
        case 'cpj':
          if(isset($_GET['id']) ){
            if($_GET['id'] == 'registration'){
              header("Location: ".BASE_URL."register");
            }
          }
          else include 'view/cpj.php';
          break;
        case 'cps':
          if(isset($_GET['id']) ){
            if($_GET['id'] == 'registration'){
              header("Location: ".BASE_URL."register");
            }
          }
          else include 'view/cps.php';
          break;
        case 'hackathon':
          if(isset($_GET['id']) ){
            if($_GET['id'] == 'registration'){
              header("Location: ".BASE_URL."register");
            }
          }
          else include 'view/hackathon.php';
          break;
        case 'hoc':
          if(isset($_GET['hoc']) ){
            if($_GET['id'] == 'registration'){
              header("Location: ".BASE_URL."register");
            }
          }
          else include 'view/hoc.php';
          break;
        case 'basket':
          if(isset($_GET['id']) ){
            if($_GET['id'] == 'registration'){
              header("Location: ".BASE_URL."register");
            }
          }
          else include 'view/basket.php';
          break;
        case 'futsal':
          if(isset($_GET['id']) ){
            if($_GET['id'] == 'registration'){
              header("Location: ".BASE_URL."register");
            }
          }
          else include 'view/futsal.php';
          break;
        case 'tigath':
          if(isset($_GET['id']) ){
            if($_GET['id'] == 'registration'){
              header("Location: ".BASE_URL."register");
            }
          }
          else include 'view/tigath.php';
          break;
        case 'gallery':
          include 'view/gallery.php';
          break;
        case 'cpFinalist':
          include 'view/cpfinalist.php';
          break;
        case 'csicFinalist':
          include 'view/csicFinalist.php';
          break;
        case 'subscribe':
          if(isset($_POST["submit"])) include 'controller/subscribe.php'; 
          break;
        case 'forgetPassword':
          if(isset($_POST["submit"])) include 'controller/forget_password.php'; 
          else include 'view/forget_password.php';
          break;
        default:
          header("Location: ".BASE_URL."index");
          break;
      }
    }
    else if(isset($_SESSION['user_fullname'])){ //masukkin list yang baru bisa diliat kalo user udah login
      switch($_GET['page']){
        case 'index':
          include 'view/index.php';
          break;
        case 'csic':
          if(isset($_GET['id']) ){
            if($_GET['id'] == 'registration'){
              if(!isset($_POST["submit"])){
                include 'controller/competition/csic_check_exist.php';
                include 'view/competition/csic.php';
              }
              else include 'controller/competition/csic.php';
            }
          }
          else include 'view/csic.php';
          break;
        case 'updateCsic':
          if(isset($_POST["submit"])){
            include 'controller/competition/csic_check_exist.php';
            include 'controller/competition/update_csic.php';
          }
          else include 'view/competition/csic.php';
          break;
        case 'proofCsic':
          if(isset($_POST["submit"])){
            include 'controller/competition/csic_check_exist.php';
            include 'controller/competition/proof_csic.php';
          }
          else include 'view/competition/csic.php';
          break;
        case 'proposalCsic':
          if(isset($_POST["submit"])){
            include 'controller/competition/csic_check_exist.php';
            include 'controller/competition/proposal_csic.php';
          }
          else include 'view/competition/csic.php';
          break;
        case 'hackathon':
          if(isset($_GET['id']) ){
            if($_GET['id'] == 'registration'){
              if(!isset($_POST["submit"])){
                include 'controller/competition/hack_check_exist.php';
                include 'view/competition/hack.php';
              }
              else include 'controller/competition/hack.php';
            }
          }
          else include 'view/hackathon.php';
          break;
        case 'updateHack':
          if(isset($_POST["submit"])){
            include 'controller/competition/hack_check_exist.php';
            include 'controller/competition/update_hack.php';
          }
          else include 'view/competition/hack.php';
          break;
        case 'proofHack':
          if(isset($_POST["submit"])){
            include 'controller/competition/hack_check_exist.php';
            include 'controller/competition/proof_hack.php';
          }
          else include 'view/competition/hack.php';
          break;
        case 'proposalHack':
          if(isset($_POST["submit"])){
            include 'controller/competition/hack_check_exist.php';
            include 'controller/competition/proposal_hack.php';
          }
          else include 'view/competition/hack.php';
          break;
        case 'cp':
          if($_GET['id'] == 'registerscpc'){
            if(!isset($_POST["submit"])){
              include 'controller/competition/cps_check_exist.php';
              include 'view/competition/cps.php';
            }
            else include 'controller/competition/cps.php';
          }
          else if($_GET['id'] == 'registerjcpc'){
            if(!isset($_POST["submit"])){
              include 'controller/competition/cpj_check_exist.php';
              include 'view/competition/cpj.php';
            }
            else include 'controller/competition/cpj.php';
          }
          break;
        case 'cpj':
          include 'controller/competition/cpj_check_exist.php';
          include 'view/cpj.php';
          break;
        case 'updatecpj':
          if(isset($_POST["submit"])){
            include 'controller/competition/cpj_check_exist.php';
            include 'controller/competition/update_cpj.php';
          }
          else include 'view/competition/cpj.php';
          break;
        case 'proofcpj':
          if(isset($_POST["submit"])){
            include 'controller/competition/cpj_check_exist.php';
            include 'controller/competition/proof_cpj.php';
          }
          else include 'view/competition/cpj.php';
          break;
        case 'cps':
          include 'controller/competition/cps_check_exist.php';
          include 'view/cps.php';
          break;
        case 'updatecps':
          if(isset($_POST["submit"])){
            include 'controller/competition/cps_check_exist.php';
            include 'controller/competition/update_cps.php';
          }
          else include 'view/competition/cps.php';
          break;
        case 'proofcps':
          if(isset($_POST["submit"])){
            include 'controller/competition/cps_check_exist.php';
            include 'controller/competition/proof_cps.php';
          }
          else include 'view/competition/cps.php';
          break;
        case 'changePassword':
          if(isset($_POST["submit"])){
            include 'controller/change_password.php';
          }
          else include 'view/change_password.php';
          break;
        case 'logout':
          include 'controller/logout.php';
          break;
        case 'home':
          if(isset($_SESSION['user_fullname'])) include 'view/home.php';
          else header("Location: ".BASE_URL);
          break;
        case 'gallery':
          include 'view/gallery.php';
          break;
        case 'subscribe':
          if(isset($_POST["submit"])) include 'controller/subscribe.php'; 
          break;
        default:
          header("Location: ".BASE_URL."index");
          break;
      }
    }
    else if(isset($_SESSION['admin_fullname'])){ //masukkin list yang baru bole diakses kalo admin udah login
      switch($_GET['page']){
        case 'home':
          include 'view/admin/home.php';
          break;
        case 'skma':
          if(!isset($_POST["submit"])){
            include 'controller/admin/showSkma.php';
            include 'view/admin/skma.php';
          }
          else include 'controller/admin/skma.php';
          break;
        case 'payment':
          if(!isset($_POST["team_id"])){
            include 'controller/admin/showPayment.php';
            include 'view/admin/payment.php';
          }
          break;
        case 'paymentApprove':
          include 'controller/admin/payment.php';
          break;
        case 'proposal':
          if(!isset($_POST["submit"])){
            include 'controller/admin/showProposal.php';
            include 'view/admin/proposal.php';
          }
          else include 'controller/admin/skma.php';
          break;
        case 'changePassword':
          if(isset($_POST["submit"])){
            include 'controller/admin/change_password.php';
          }
          else include 'view/admin/change_password.php';
          break;
        case 'logout':
          include 'controller/logout.php';
          break;
        default:
          header("Location: ".BASE_URL."home");
          break;
      }
    }
  } 
?>