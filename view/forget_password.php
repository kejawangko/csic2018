<?php include 'view/include/header_forget.php'; ?>

<!-- #single-blog-post -->
<section id="single-blog-post">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 comment-form-section">
                <div class="form-title text-center">
                    <h1>Forget Password</h1>
                </div>
                <div class="comment-form">
                    <form action="<?php echo BASE_URL;?>forgetPassword" method="post" role="form">
                        <ul class="clearfix">
                            <li class="full"><input name="email" type="text" placeholder="Your Email" required></li>
                            <?php
                                if(isset($_SESSION['forget_password_success'])){
                                  echo "<div style='margin-left:26%;margin-top:-3%'>".$_SESSION['forget_password_success']."</div>";
                                  $_SESSION['forget_password_success'] = "";
                                }
                            ?>
                            <li class="full"><button style="left: -4%;" name="submit" type="submit" class="hvr-bounce-to-right">Send Email</button></li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /#single-blog-post -->

<?php include "view/include/footer.php" ?>
<?php include "includes/script.php" ?>

</body>
</html>