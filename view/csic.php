<?php
  include 'view/include/header_csic.php';
?>

<!-- #page-title -->

<!-- /#page-title -->


<!-- #single-blog-post -->
<section id="single-blog-post">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<!-- .content-holder -->
				<div class="content-holder">
					<!-- .img-holder -->
					<div class="img-holder">
						<img src="<?php echo BASE_URL;?>template/img/csic.jpg" alt="" style="width:50%;">
					</div>
					<!-- /.img-holder -->
					<!-- .post-title -->
					<div class="post-title">
						<h1>Course-Net CSIC 2017</h1>
					</div>
					<!-- /.post-title -->

					<!-- .content -->
					<div class="content">
						<p align="justify">Computer Science Innovative Challenge (CSIC) is a creative and innovative software program competition for undergraduate students. Course-Net CSIC 2017 challenges you to turn your ideas into an innovative application program as an answer to the tech industry challenge. Course-Net CSIC 2017 is now opened to all undergraduate students in Indonesia.</p>
					</div>

					<div class="page-title" style="margin-top:5%;">
						<h1>CSIC Finalist &#9759;</h1>
					</div>
					

					<div style="margin-top:5%;">
						<ul>
							<li>
								<a href="<?php echo BASE_URL;?>csicFinalist" class="colored hvr-bounce-to-right" id="rulebook" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;margin-bottom: 1%">
									VIEW FINALIST
								</a>
							</li>
							<li>
								<a href="<?php echo BASE_URL;?>storage/Requirements-Final.pdf" class="colored hvr-bounce-to-right" id="rulebook" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;margin-bottom: 1%">
									FINAL PREPARATION
								</a>
							</li>
							<li>
								<?php if(!isset($_SESSION['csic_team']['payment_proof'])){ ?>
								<a href="<?php echo BASE_URL;?>csic/registration" class="colored hvr-bounce-to-right" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;"> VIEW MY TEAM </a>
								<?php } else if(isset($_SESSION['csic_team']['payment_proof'])) {?>
								<a href="<?php echo BASE_URL;?>csic/registration" class="colored hvr-bounce-to-right" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;"> VIEW MY TEAM </a>
								<?php }?>
							</li>
							<li>
								<a href="<?php echo BASE_URL;?>storage/Rulebook Course-Net CSIC.pdf" class="colored hvr-bounce-to-right" id="rulebook" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;margin-top: 1%">
									RULEBOOK
								</a>
							</li>
						</ul>
					</div>

					<!-- /.content -->
				</div>
				<!-- /.content-holder -->

			</div>
		</div>
	</div>
</section>
<!-- /#single-blog-post -->

<section id="single-blog-post">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<!-- .content-holder -->
				<div class="content-holder">

					<div class="row">
					    <div class="col-md-12">
					        <div>
					            <h3>Timeline</h3>
					        </div>
				            <ul class="timeline">
								<li>
				                  <div class="posted-date">
				                    <span class="month">30 August 2017 until 7 October 2017</span>
				                  </div>
				                  <div class="timeline-panel">
				                    <div class="timeline-content">
				                      <div class="timeline-heading">
				                        <h3>Registration</h3>
				                      </div><!-- /timeline-heading -->
				                    </div> <!-- /timeline-content -->
				                  </div><!-- /timeline-panel -->
				                </li>
								<li class="timeline-inverted">
				                  <div class="posted-date">
				                    <span class="month">29 October 2017</span>
				                  </div>
				                  <div class="timeline-panel">
				                    <div class="timeline-content">
				                      <div class="timeline-heading">
				                        <h3>Prototype and Proposal Deadline</h3>
				                      </div><!-- /timeline-heading -->
				                    </div> <!-- /timeline-content -->
				                  </div><!-- /timeline-panel -->
				                </li>
				                <li>
				                  <div class="posted-date">
				                    <span class="month">7 November 2017</span>
				                  </div>
				                  <div class="timeline-panel">
				                    <div class="timeline-content">
				                      <div class="timeline-heading">
				                        <h3>Finalist Announcement</h3>
				                      </div><!-- /timeline-heading -->
				                    </div> <!-- /timeline-content -->
				                  </div><!-- /timeline-panel -->
				                </li>
								<li class="timeline-inverted">
				                  <div class="posted-date">
				                    <span class="month">7 December 2017</span>
				                  </div>
				                  <div class="timeline-panel">
				                    <div class="timeline-content">
				                      <div class="timeline-heading">
				                        <h3>Presentation and Final Judging</h3>
				                      </div><!-- /timeline-heading -->
				                    </div> <!-- /timeline-content -->
				                  </div><!-- /timeline-panel -->
				                </li>
				            </ul>
					    </div>
					</div>
					<!-- /row -->
					<!-- /.content -->
					
					<!-- .post-meta -->
					<!-- <div class="post-meta">
						Posted by <a href="#">Jhone</a> on <a href="#">dec 24,2014</a>
					</div> -->
					<!-- /.post-meta -->
				</div>
				<!-- /.content-holder -->

			</div>
		</div>
	</div>
</section>

<section id="event-sponsor">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title">
						<h1>EVENT SPONSOR</h1>
					</div>
				</div>
			</div>
			<div class="row sponsor-logo-row">
				<div class="col-lg-12">
					<div class="item col-lg-4">
						<a href="http://www.course-net.com/" target="_blank" class="hvr-underline-reveal"><img src="<?php echo BASE_URL;?>template/img/sponsor-logo/coursenet.png" alt=""></a>
					</div>
				</div>
			</div>
			<div class="row sponsor-logo-row">
				<div class="col-lg-12">
					<div class="item col-lg-3" >
						<a href="http://www.kontan.co.id/" target="_blank" class="hvr-underline-reveal"><img src="<?php echo BASE_URL;?>template/img/sponsor-logo/kontan.png" alt=""></a>
					</div>					
				</div>
			</div>
			<hr>
		</div>
	</section>

<!-- /#event-sponsor -->

<?php include "view/include/footer.php" ?>
<?php include 'includes/script.php' ?>

</body>
</html>