<?php include 'view/include/header_cPass.php'; ?>
<body>

    <section id="single-blog-post">
        <div class="container">
            <div class="row" style="margin-top:-5%">
                <div class="col-lg-12 comment-form-section">
                    <div class="form-title">
                        <h1>Change Password</h1>
                    </div>
                    <div class="comment-form">
                        <form action="<?php echo BASE_URL."changePassword";?>" method="post" style="margin-left:-5%;" enctype="multipart/form-data">
                            <?php if(isset($_SESSION['change_password']) && !isset($_POST['submit'])){
                            ?>
                                <strong><?php echo "<text style='color:green;'>".$_SESSION['change_password']."</text>"; ?></strong>
                            <?php
                                $_SESSION['change_password'] = "";
                            }?>
                            <ul>
                                <li class="full" style="margin-left:-25%;">
                                    <input type="password" name="oldPassword" id="oldPassword" placeholder="Your Old Password" required">
                                </li>
                                <li class="full" style="margin-left:-25%;">
                                    <input type="password" name="newPassword" id="newPassword" placeholder="Your New Password" required>
                                </li>
                                <li class="full" style="margin-left:-25%;">
                                    <input type="password" name="newPasswordConfirmation" id="newPasswordConfirmation" placeholder="Confirm Your New Password" required onchange="myFunction()">
                                    <div id="confError" style="color:red;font-size: 10pt;margin-left: 25%;"></div>
                                </li>
                                <li>
                                    <button name="submit" type="submit" class="btn hvr-bounce-to-right" style="margin-left:-5%;margin-top:2%;width:25%;letter-spacing: 2px;">Change Password
                                    </button>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- /#single-blog-post -->

    <?php include "view/include/footer.php" ?>

    <?php include "includes/script.php" ?>

    <script>
        function myFunction() {
            var newPass = $('#newPassword').val();
            var confPass = $('#newPasswordConfirmation').val();
            if(newPass.localeCompare(confPass)== -1){
                document.getElementById("confError").innerHTML = "*New Password and Confirm Password Doesn't Match";
            }
        }
    </script>

</body>
</html>