<?php include 'view/include/header_gallery.php'; ?>

<body>



	<!-- #upcoming-event -->
	<section id="upcoming-event">
		<div class="container">
			<div class="row">
				<div class="col-lg-7 col-md-7 col-sm-7">
					<div class="section-title">
						<h1>CSIC 2016 & CSIC 2017</h1>
						<p>Just a sneak peek of our last event.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="tab-content-wrap row">
						<?php
							for ($i=1; $i <=59 ; $i++) { ?>
							<div class="col-lg-3 col-md-4 col-sm-6 mix csic">
								<div class="row gallery-image">
									<a class="fancybox" href="<?php echo BASE_URL;?>template/img/gallery_full/csic-<?php echo $i;?>.jpg">
										<div>
											<img src="<?php echo BASE_URL;?>template/img/gallery_full/csic-<?php echo $i;?>-272x170.jpg" alt="">
										</div>
									</a>
								</div>
							</div>
						<?php		
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /#upcoming-event -->
	
<?php include "view/include/footer.php" ?>

	<?php include "includes/script.php" ?>
</body>