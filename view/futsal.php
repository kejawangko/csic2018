<?php
  include 'view/include/header_futsal.php';
?>

<!-- #page-title -->

<!-- /#page-title -->


<!-- #single-blog-post -->
<section id="single-blog-post">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<!-- .content-holder -->
				<div class="content-holder">
					<!-- .img-holder -->
					<div class="img-holder">
						<img src="<?php echo BASE_URL;?>template/img/futsal.jpg" alt="" style="width:50%;">
					</div>
					<!-- /.img-holder -->
					<!-- .post-title -->
					<div class="post-title">
						<h1>Futsal</h1>
					</div>
					<!-- /.post-title -->

					<!-- .content -->
					<div class="content">
						<p align="justify">Not only presenting academic competition, FesTIval also presents Futsal for students who do favor in this sport. FesTIval-Futsal upholds the spirit of sportsmanship in every game. Undergraduate students with field of engineering and informatics studies in Jabodetabek may join this competition. So form your team now as FesTIval-Futsal is only limited to 16 teams!</p>
					</div>
					<!-- /.content -->
					
					<!-- .post-meta -->
					<!-- <div class="post-meta">
						Posted by <a href="#">Jhone</a> on <a href="#">dec 24,2014</a>
					</div> -->
					<!-- /.post-meta -->

					<!-- <div class="page-title" style="margin-top:5%;">
						<h1 style="font-size:48pt;">COMING SOON!</h1>
					</div> -->
					<div style="margin-top:5%;">
						<ul>
							<li>
								<a href="https://docs.google.com/forms/d/e/1FAIpQLScLTIA7p3V0dLX2kMV0RP1zP8p8mWSwDDKih-qJhQ8zDVi_TA/viewform?c=0&w=1" class="colored hvr-bounce-to-right" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;"> REGISTER NOW </a>
							</li>
							<li>
								<a href="<?php echo BASE_URL;?>storage/Rulebook Futsal FesTIval 2017.pdf" class="colored hvr-bounce-to-right" id="rulebook" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;margin-top: 1%">
									RULEBOOK
								</a>
							</li>
						</ul>
					</div>
				</div>
				<!-- /.content-holder -->

			</div>
		</div>
	</div>
</section>
<!-- /#single-blog-post -->

<!-- #event-sponsor -->
<!-- <section id="event-sponsor">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="section-title">
					<h1>EVENT SPONSOR</h1>
					<p>tar taro logo sponsor disini bagus kayanya...</p>
				</div>
			</div>
		</div>
		<div class="row sponsor-logo-row">
			<div class="col-lg-12">
				<ul class="sponsor-logo">
					<li>
						<div class="item"><img src="img/sponsor-logo/1.png" alt=""></div>
					</li>
					<li>
						<div class="item"><img src="img/sponsor-logo/2.png" alt=""></div>
					</li>
					<li>
						<div class="item"><img src="img/sponsor-logo/3.png" alt=""></div>
					</li>
					<li>
						<div class="item"><img src="img/sponsor-logo/4.png" alt=""></div>
					</li>
				</ul>
			</div>
		</div>
	</div> -->
</section>
<!-- /#event-sponsor -->

<?php include "view/include/footer.php" ?>
<?php include 'includes/script.php' ?>

</body>
</html>