<?php
  include 'view/include/header_hoc.php';
?>

<!-- #page-title -->

<!-- /#page-title -->


<!-- #single-blog-post -->
<section id="single-blog-post">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<!-- .content-holder -->
				<div class="content-holder">
					<!-- .img-holder -->
					<div class="img-holder">
						<img src="<?php echo BASE_URL;?>template/img/hoc.jpg" alt="" style="width:50%;">
					</div>
					<!-- /.img-holder -->
					<!-- .post-title -->
					<div class="post-title">
						<h1>Hour of Code</h1>
					</div>
					<!-- /.post-title -->

					<!-- .content -->
					<div class="content">
						<p align="justify">As the world needs all people to have strong logical thinking ability, Hour of Code (HoC) is a fun educative way of introducing and teaching programming logic to high school students. HoC offers a great opportunity for students of UMN Computer Science to participate as volunteers.</p>
					</div>
					<!-- /.content -->
					
					<!-- .post-meta -->
					<!-- <div class="post-meta">
						Posted by <a href="#">Jhone</a> on <a href="#">dec 24,2014</a>
					</div> -->
					<!-- /.post-meta -->
					<div style="margin-top:5%;">
						<ul>
							<li>
								<a href="https://docs.google.com/forms/d/e/1FAIpQLScix418KdOZK6T1_OCDvHrT2wnsoojyw0VAC8XwfTt-bogGcQ/viewform?c=0&w=1" class="colored hvr-bounce-to-right" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;"> REGISTER NOW </a>
							</li>
						</ul>
					</div>
				</div>
				<!-- /.content-holder -->

			</div>
		</div>
	</div>
</section>
<!-- /#single-blog-post -->

<!-- #event-sponsor -->
<!-- <section id="event-sponsor">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="section-title">
					<h1>EVENT SPONSOR</h1>
					<p>tar taro logo sponsor disini bagus kayanya...</p>
				</div>
			</div>
		</div>
		<div class="row sponsor-logo-row">
			<div class="col-lg-12">
				<ul class="sponsor-logo">
					<li>
						<div class="item"><img src="img/sponsor-logo/1.png" alt=""></div>
					</li>
					<li>
						<div class="item"><img src="img/sponsor-logo/2.png" alt=""></div>
					</li>
					<li>
						<div class="item"><img src="img/sponsor-logo/3.png" alt=""></div>
					</li>
					<li>
						<div class="item"><img src="img/sponsor-logo/4.png" alt=""></div>
					</li>
				</ul>
			</div>
		</div>
	</div> -->
</section>
<!-- /#event-sponsor -->

<?php include "view/include/footer.php" ?>
<?php include 'includes/script.php' ?>

</body>
</html>