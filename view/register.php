<?php include 'view/include/header_register.php'; ?>


<!-- #single-blog-post -->
<section id="single-blog-post">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 comment-form-section">
                <div class="form-title text-center">
                    <h1>Register</h1>
                </div>
                <?php
                    if(isset($_SESSION['database_error']) && !isset($_POST['submit'])){
                        echo $_SESSION['database_error'];
                        $_SESSION['database_error'] = '';
                    }
                    else if(isset($_SESSION['register_success']) && !isset($_POST['submit'])){
                        echo $_SESSION['register_success'];
                        $_SESSION['register_success'] = '';
                    }
                ?>
                <div class="comment-form">
                    <form action="<?php echo BASE_URL;?>register" method="post">
                        <ul class="clearfix">
                            <li class="full"><input name="name" type="text" placeholder="Your Name" required></li>
                            <li class="full"><input name="email" type="email" placeholder="Your Email" required></li>
                            <li class="full"><input name="password" type="password" placeholder="Your Password" id="myPassword" required></li>
                            <li class="full"><input name="password_confirmation" type="password" placeholder="Confirm Your Password" id="confirmPassword" onchange="myFunction()" required>
                            <div id="confError" style="color: red; font-size: 10pt;margin-left:25%;"></div><br>
                                <?php
                                    if(isset($_SESSION['password_unmatch'])){
                                        echo "<text style='color:red;width:50%;margin-left:25%;font-size:10pt;'>".$_SESSION['password_unmatch']."</text>";
                                        $_SESSION['password_unmatch'] = '';
                                    }
                                ?>
                            </li>
                            <div>
                                <?php
                                    if(isset($_SESSION['error'])){
                                        echo "<text style='color:red;width:50%;margin-left:25%;font-size:10pt;'>".$_SESSION['error']."<text>";
                                        $_SESSION['error'] = '';
                                    }
                                ?>
                            </div>
                            <li class="full"><button name="submit" type="submit" class="hvr-bounce-to-right">Register</button></li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /#single-blog-post -->

<?php include 'view/include/footer.php'; ?>
<?php include "includes/script.php" ?>

    <script>
        function myFunction() {
            var newPass = $('#myPassword').val();
            var confPass = $('#confirmPassword').val();
            if(newPass.localeCompare(confPass)== -1){
                document.getElementById("confError").innerHTML = "*Your Password and Confirm Password are Missmatch";
            }
            else if(newPass.localeCompare(confPass) == 0){
                document.getElementById("confError").style.display = "none";
            }
        }
    </script>

</body>
</html>