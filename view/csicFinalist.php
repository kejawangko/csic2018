<?php

  include 'view/include/header_csicFinalist.php';

?>



<style>

  table, td, th {

    text-align: center;

  }



  table {

      border-collapse: collapse;

      width: 100%;

  }



  th, td {

      padding: 15px;

  }



  th{

  	border: 1px solid #bbb;

  	font-size: 15pt;

  }

  td{

  	border: 1px solid #bbb;

  	font-size: 12pt;

  }

</style>



<!-- #page-title -->



<!-- /#page-title -->





<!-- #single-blog-post -->

<section id="single-blog-post">

	<div class="container">

		<div class="row">

			<div class="col-lg-12 text-center">

				<!-- .content-holder -->

				<div class="content-holder">

					<!-- .img-holder -->

					<div class="img-holder">

						<img src="<?php echo BASE_URL;?>template/img/csicFinalist.jpg" alt="" style="width:50%;">

					</div>

					<!-- /.img-holder -->

					<!-- .post-title -->

					<div class="post-title">

						<h2>Road to FesTIval 2017</h2>

						<h2><strong>Final CSIC Contest!</strong></h2>

					</div>

					<!-- /.post-title -->



					<!-- .content -->

					<div class="content" style="margin-top:70px;">

						<p align="center" style="font-size: 14pt;">We would like to thank you for participating in preliminary round. <br> Here are the <strong>Finalists of Computer Science Innovative Challenge</strong> contest! </p>

						<p align="center" style="font-size: 20pt;text-transform: uppercase;"><strong>Congratulations for the finalists!</strong></p>

						<p align="center" style="font-size: 14pt;">The FesTIval 2017 Final CSIC contest will be <strong>held on</strong> : </p>

						<p align="center" style="font-size: 23pt;text-transform: uppercase;"><strong>Thursday, December 7th 2017 <br><br> 09.00 - Finish<br><br><text style="font-size: 15pt;"><br>At Universitas Multimedia Nusantara, Gading Serpong</text></strong></p>

						<p align="center" style="font-size: 14pt;">Every step you take <text style="color:red"><strong>counts</strong></text>. So, don't stop <text style="color:blue;"><strong>breaking legs</strong></text>! Always prepare for the <text style="color:orange;"><strong>best</strong></text>! </p>



					</div>



					<div class="post-title" style="margin-top:100px;">

						<table style="width: 55%;" align="center">

							<tr>

								<td style="border: none !important;"><h1>CSIC Finalist</h1></td>

							</tr>

							<tr>

								<td style="border: none !important;">

									<table style="width: 100%;border: 1px solid #bbb;margin:auto;" >

								        <thead>

								          <tr>

								            <th>Team Name</th>

								          </tr>

								        </thead>

								        <tbody>

								        	<tr><td>infinite loop</td></tr>

								        	<tr><td>code diggers</td></tr>

								        	<tr><td>teamfausta</td></tr>

								        	<tr><td>basing</td></tr>

								        	<tr><td>PPTI_BCA_TricMinder</td></tr>

								        	<tr><td>alien</td></tr>

								        	<tr><td>ThankYouCSIC</td></tr>

								        	<tr><td>Illuminate</td></tr>

								        	<tr><td>PPTI_BCA_JAGUNG</td></tr>

								        	<tr><td>PPTI_BCA_AstroRhythm</td></tr>

								        </tbody>

								    </table>

								</td>

							</tr>

						</table>

					</div>



					

				</div>

				<!-- /.content-holder -->



			</div>

		</div>

	</div>

</section>

<!-- /#single-blog-post -->







<section id="event-sponsor">

		<div class="container">

			<div class="row">

				<div class="col-lg-12">

					<div class="section-title">

						<h1>EVENT SPONSOR</h1>

						<!-- <p>Thank you for sponsor and media partner.</p> -->

						<!-- <hr> -->

					</div>

				</div>

			</div>

			<div class="row sponsor-logo-row">

				<div class="col-lg-12">

					<div class="item col-lg-4">

						<a href="http://www.course-net.com/" target="_blank" class="hvr-underline-reveal"><img src="<?php echo BASE_URL;?>template/img/sponsor-logo/coursenet.png" alt=""></a>

					</div>

				</div>

			</div>

			<div class="row sponsor-logo-row">

				<div class="col-lg-12">

					<div class="item col-lg-3" >

						<a href="http://www.kontan.co.id/" target="_blank" class="hvr-underline-reveal"><img src="<?php echo BASE_URL;?>template/img/sponsor-logo/kontan.png" alt=""></a>

					</div>					

				</div>

			</div>

			<hr>

		</div>

	</section>



<?php include "view/include/footer.php" ?>



<?php include 'includes/script.php' ?>



</body>

</html>