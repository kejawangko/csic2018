<?php
  include 'view/include/header_cpFinalist.php';
?>

<style>
  table, td, th {
    text-align: center;
  }

  table {
      border-collapse: collapse;
      width: 100%;
  }

  th, td {
      padding: 15px;
  }

  th{
  	border: 1px solid #bbb;
  	font-size: 15pt;
  }
  td{
  	border: 1px solid #bbb;
  	font-size: 12pt;
  }
</style>

<!-- #page-title -->

<!-- /#page-title -->


<!-- #single-blog-post -->
<section id="single-blog-post">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<!-- .content-holder -->
				<div class="content-holder">
					<!-- .img-holder -->
					<div class="img-holder">
						<img src="<?php echo BASE_URL;?>template/img/final_cp.png" alt="" style="width:50%;">
					</div>
					<!-- /.img-holder -->
					<!-- .post-title -->
					<div class="post-title">
						<h2>Road to FesTIval 2017</h2>
						<h2><strong>Final Competitive Programming Contest!</strong></h2>
					</div>
					<!-- /.post-title -->

					<!-- .content -->
					<div class="content" style="margin-top:70px;">
						<p align="center" style="font-size: 14pt;">We would like to thank you for participating in preliminary round. <br> Here are the <strong>Finalists of Senior and Junior Competitive Programming</strong> contest! </p>
						<p align="center" style="font-size: 20pt;text-transform: uppercase;"><strong>Congratulations for the finalists!</strong></p>
						<p align="center" style="font-size: 14pt;">The FesTIval 2017 Final Competitive Programming contest will be <strong>held on</strong> : </p>
						<p align="center" style="font-size: 23pt;text-transform: uppercase;"><strong>Saturday, October 7th 2017 <br><br> 08.00 - 17.30<br><br><text style="font-size: 15pt;">At Universitas Multimedia Nusantara, Gading Serpong</text></strong></p>
						<p align="center" style="font-size: 14pt;">Every step you take <text style="color:red"><strong>counts</strong></text>. So, don't stop <text style="color:blue;"><strong>breaking legs</strong></text>! Always prepare for the <text style="color:orange;"><strong>best</strong></text>! </p>
						<p align="center" style="font-size: 14pt;">Please <strong style="color:purple">check your email</strong> to get further final round information. See you on final! </p>

					</div>

					<div class="post-title" style="margin-top:100px;">
						<table >
							<tr>
								<td style="border: none !important;"><h1>Senior Competitive Programming Finalist</h1></td>
								<td style="border: none !important;"><h1>Junior Competitive Programming Finalist</h1></td>
							</tr>
							<tr>
								<td style="border: none !important;">
									<table style="width: 100%;border: 1px solid #bbb;margin:auto;" >
								        <thead>
								          <tr>
								            <th>Team Name</th>
								          </tr>
								        </thead>
								        <tbody>
								        	<tr><td>AgreeWTF</td></tr>
								        	<tr><td>Ainge WF</td></tr>
								        	<tr><td>Air Garam</td></tr>
								        	<tr><td>BanganTurun</td></tr>
								        	<tr><td>BeeSquad</td></tr>
								        	<tr><td>BOOM_mau_dinotice_juga</td></tr>
								        	<tr><td>CarryMore</td></tr>
								        	<tr><td>Codeseeker</td></tr>
								        	<tr><td>Eucatastrophe</td></tr>
								        	<tr><td>fooCP</td></tr>
								        	<tr><td>GaPerluCompile</td></tr>
								        	<tr><td>Icarus</td></tr>
								        	<tr><td>LInearTimeSolver</td></tr>
								        	<tr><td>Mengisi Perut</td></tr>
								        	<tr><td>Pyon!</td></tr>
								        	<tr><td>RemahanMalkist</td></tr>
								        	<tr><td>Takada Clan</td></tr>
								        	<tr><td>Tam</td></tr>
								        	<tr><td>Terangkanlah</td></tr>
								        	<tr><td>theProOne</td></tr>
								        	<tr><td>tobeDetermined</td></tr>
								        	<tr><td>Tsugi no Reberu</td></tr>
								        	<tr><td>Unyu Limit Exceeded 3</td></tr>
								        	<tr><td>Urange CP</td></tr>
								        	<tr><td>vjudge1</td></tr>
								        </tbody>
								    </table>
								</td>
								<td style="border: none !important;vertical-align: top">
									<table style="width: 100%;border: 1px solid #bbb;margin:auto;">
								        <thead>
								          <tr>
								            <th>Participant Name</th>
								          </tr>
								        </thead>
								        <tbody>
								        	<tr><td>Arbi Arifin</td></tr>
								        	<tr><td>Benedictus Visto Kartika</td></tr>
								        	<tr><td>Devi Priscilla Kho</td></tr>
								        	<tr><td>Freddy Simatupang</td></tr>
								        	<tr><td>BeeSquad</td></tr>
								        	<tr><td>Hermanto</td></tr>
								        	<tr><td>Jeffrey Sentosa</td></tr>
								        	<tr><td>Kathy F Ijaya</td></tr>
								        	<tr><td>Kenny Winata</td></tr>
								        	<tr><td>Kezia Sulami</td></tr>
								        	<tr><td>Kristanto</td></tr>
								        	<tr><td>Marcellino Jason</td></tr>
								        	<tr><td>Marco Halim</td></tr>
								        	<tr><td>Muhammad Faqih Wijaya</td></tr>
								        	<tr><td>Naomi Evelyn</td></tr>
								        	<tr><td>Rifqi Eka Hardianto</td></tr>
								        	<tr><td>Vincent Phandiarta</td></tr>
								        	<tr><td>Wesley Winardi</td></tr>
								        </tbody>
								    </table>
								</td>
							</tr>
						</table>
					</div>

					
				</div>
				<!-- /.content-holder -->

			</div>
		</div>
	</div>
</section>
<!-- /#single-blog-post -->



<!-- #event-sponsor -->
	<section id="event-sponsor">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title">
						<h1>EVENT SPONSOR</h1>
						<!-- <p>Thank you for sponsor and media partner.</p> -->
						<!-- <hr> -->
					</div>
				</div>
			</div>
			<div class="row sponsor-logo-row">
				<div class="col-lg-12">
					<div class="item col-lg-4">
						<a href="http://www.phi-integration.com/" target="_blank" class="hvr-underline-reveal"><img src="<?php echo BASE_URL;?>template/img/sponsor-logo/phi.png" alt=""></a>
					</div>
				</div>
			</div>
			<div class="row sponsor-logo-row">
				<div class="col-lg-12">
					<div class="item col-lg-3" >
						<a href="https://www.dellemc.com/id-id/index.htm/" target="_blank" class="hvr-underline-reveal"><img src="<?php echo BASE_URL;?>template/img/sponsor-logo/dell.png" alt=""></a>
					</div>
					<div class="item col-lg-3">
						<a href="https://www.synnexmetrodata.com/" target="_blank" class="hvr-underline-reveal"><img src="<?php echo BASE_URL;?>template/img/sponsor-logo/synex.png" alt=""></a>
					</div>
					
				</div>
			</div>
			<hr>
		</div>
	</section>
<!-- /#event-sponsor -->

<?php include "view/include/footer.php" ?>

<?php include 'includes/script.php' ?>

</body>
</html>