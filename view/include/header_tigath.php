<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>FesTIval</title>
  
  <!-- Responsive Meta Tag -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- main stylesheet -->
  <?php include 'includes/link.php'; ?>

</head>
<body>

  <header id="header">
    <div class="container">
      <div class="row">
        <!-- .logo -->
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 logo">
          <a href="<?php echo BASE_URL;?>index"><img src="<?php echo BASE_URL;?>template/img/resources/logo.png" alt="Logo Image"></a>
        </div>
        <!-- /.logo -->

        <!-- .mainmenu-container -->
        <nav class="col-lg-9 col-md-9 col-sm-6 col-xs-6 mainmenu-container">
          <!-- <button class="nav-toggler">Navigation <i class="fa fa-bars"></i></button>     -->
          <ul class="mainmenu clearfix">
            <!-- <li class="nav-closer"><i class="fa fa-close"></i></li> -->
            <li>
              <a><h2>TI Gathering UMN</h2></a>
            </li>
            
          </ul>
        </nav>
        <!-- /.mainmenu-container -->
      </div>
    </div>
  </header>
  <div id="cover-title" class="gradient-overlay" style="height: 120px;">

  </div>
</body>