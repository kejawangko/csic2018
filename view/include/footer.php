<!-- footer -->
	<footer>
		<div class="container">
			<div class="row" id="contact-bawah">
				<!-- .footer-widget -->
				<div class="col-lg-4 col-md-4 col-sm-6 footer-widget about-widget" >
					<img src="<?php echo BASE_URL;?>template/img/resources/logo.png" alt="Footer Logo">
					<p align="justify">An annual 3-month event organized by department of Computer Science of UMN.</p>
					<!-- about festival here -->
					<br>
					<p style="color: black">Contact Person</p>
					<p style="color: black"><i class="fa fa-phone" style="font-size:24px"></i>&nbsp 021-5422-0808 (3307)</p>
					<p style="color: black"><i class="fa fa-envelope" style="font-size:24px"></i>&nbsp farica@umn.ac.id</p>
				</div>
				<!-- /.footer-widget -->
				<div class="col-lg-8 col-md-2 col-sm-6 footer-widget menu-widget">
					<div class="map-area">
						<div class="map">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.0536621189462!2d106.61609431463829!3d-6.256661495471256!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69fc7b260ca1ed%3A0xb83752eec7c57ddd!2sUniversitas+Multimedia+Nusantara!5e0!3m2!1sen!2sid!4v1489216755656" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- /footer -->
	
	<!-- #bottom-bar -->
	<section id="bottom-bar">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<p style="color:#fcaf1a !important;">&copy; 2018 <a href="<?php echo BASE_URL;?>index" style="color: #1a0758 !important; font-weight: bold !important">CSIC</a> | ALL RIGHTS RESERVED</p>
				</div>
			</div>
		</div>
	</section>
	<!-- /#bottom-bar -->
