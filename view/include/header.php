<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Course-Net CSIC 2018</title>
  
  <!-- Responsive Meta Tag -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- main stylesheet -->
  <?php include 'includes/link.php'; ?>
  

</head>
<body>

<header id="header">
    <div class="container">
      <div class="row">
        <!-- .logo -->
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 logo">
          <a href="<?php echo BASE_URL;?>index"><img style="width:40%;margin-left: 40% !important;" src="<?php echo BASE_URL;?>template/img/resources/logo.png" alt="Logo Image"></a>
        </div>
        <!-- /.logo -->

        <!-- .mainmenu-container -->
        <nav class="col-lg-9 col-md-9 col-sm-6 col-xs-6 mainmenu-container">
          <button class="nav-toggler"><i class="fa fa-bars"></i></button>    
          <ul class="mainmenu clearfix">
            <li class="nav-closer"><i class="fa fa-close"></i></li>
            <!-- <li class="scrollToLink" id="home">
              <a href="#banner">HOME</a>
            </li> -->
            <li class="scrollToLink" id="about_us"><a href="#about-us">HOME</a></li>
            <li class="scrollToLink" id="event"><a href="#upcoming-event">EVENT</a></li>            
            <li class="scrollToLink" id="galery"><a href="#gallery">GALLERY</a></li>
            <li class="scrollToLink" id="sponsor"><a href="#event-sponsor">SPONSOR</a></li>
            <li class="scrollToLink" id="contact_"><a href="#contact-bawah">CONTACT</a></li>
            <li class="scrollToLink" id="account"><a href=""><?php if(isset($_SESSION['user_fullname'])) echo $_SESSION['user_fullname']; else echo "ACCOUNT";?></a>
              <ul class="submenu">
              <?php if(isset($_SESSION['user_id'])) {?>
                <li><a href="<?php echo BASE_URL;?>changePassword">MY ACCOUNT</a></li>
                <li><a href="<?php echo BASE_URL;?>logout">LOGOUT</a></li>
              <?php } else {?>
                <li><a href="<?php echo BASE_URL;?>register">REGISTER</a></li>
                <li><a href="<?php echo BASE_URL;?>login">LOGIN</a></li>
              <?php }?>
              </ul>
            </li>
          </ul>
        </nav>
        <!-- /.mainmenu-container -->
      </div>
    </div>
  </header>