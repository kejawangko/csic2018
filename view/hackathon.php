

<?php
  include 'view/include/header_hack.php';
?>

<!-- #page-title -->

<!-- /#page-title -->


<!-- #single-blog-post -->
<section id="single-blog-post">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<!-- .content-holder -->
				<div class="content-holder">
					<!-- .img-holder -->
					<div class="img-holder">
						<img src="<?php echo BASE_URL;?>template/img/hack.png" alt="" style="width:50%;">
					</div>
					<!-- /.img-holder -->
					<!-- .post-title -->
					<div class="post-title">
						<h1>Hackathon 2.0</h1>
					</div>
					<!-- /.post-title -->

					<!-- .content -->
					<div class="content">
						<p align="justify">Hackathon 2.0 denotes the second year of hackathon competition held by the Association of Computer Science Students (HIMTI) of UMN. The objective of hackathon is to challenge you to create and develop a software within 24 hours marathon, as a solution to a concrete problem. Hackathon 2.0 will be held in UMN campus. This competition is opened to all undergraduate students in Indonesia.</p>
					</div>

					<div style="margin-top:5%;">
						<ul>
							<li>
								<?php if(!isset($_SESSION['hack_team']['payment_proof'])){ ?>
								<a href="<?php echo BASE_URL;?>hackathon/registration" class="colored hvr-bounce-to-right" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;"> REGISTER NOW </a>
								<?php } else if(isset($_SESSION['hack_team']['payment_proof'])) {?>
								<a href="<?php echo BASE_URL;?>hackathon/registration" class="colored hvr-bounce-to-right" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;"> VIEW MY TEAM </a>
								<?php }?>
							</li>
							<li>
								<a href="<?php echo BASE_URL;?>storage/Rulebook Hackathon 2.0.pdf" class="colored hvr-bounce-to-right" id="rulebook" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;margin-top: 1%">
									RULEBOOK
								</a>
							</li>
						</ul>
					</div>

					<!-- <div class="page-title" style="margin-top:5%;">
						<h1 style="font-size:48pt;">COMING SOON!</h1>
					</div> -->
					<div style="margin-top:5%;">
						<ul>
							<li>
							<!-- <a href="<?php echo BASE_URL;?>csic/registration" class="colored hvr-bounce-to-right" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;"> REGISTER NOW </a> -->

							
							</li>
						</ul>
					</div>
				</div>
				<!-- /.content-holder -->

			</div>
		</div>
	</div>
</section>

<section id="single-blog-post">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<!-- .content-holder -->
				<div class="content-holder">

					<div class="row">
					    <div class="col-md-12">
					        <div>
					            <h3>Timeline</h3>
					        </div>
				            <ul class="timeline">
								<li>
				                  <div class="posted-date">
				                    <span class="month">1 November 2017 until 19 November 2017</span>
				                  </div>
				                  <div class="timeline-panel">
				                    <div class="timeline-content">
				                      <div class="timeline-heading">
				                        <h3>Registration</h3>
				                      </div><!-- /timeline-heading -->
				                    </div> <!-- /timeline-content -->
				                  </div><!-- /timeline-panel -->
				                </li>
								<li class="timeline-inverted">
				                  <div class="posted-date">
				                    <span class="month">26 November 2017</span>
				                  </div>
				                  <div class="timeline-panel">
				                    <div class="timeline-content">
				                      <div class="timeline-heading">
				                        <h3>Finalist Announcement</h3>
				                      </div><!-- /timeline-heading -->
				                    </div> <!-- /timeline-content -->
				                  </div><!-- /timeline-panel -->
				                </li>
				                <li>
				                  <div class="posted-date">
				                    <span class="month">2 until 3 December 2017</span>
				                  </div>
				                  <div class="timeline-panel">
				                    <div class="timeline-content">
				                      <div class="timeline-heading">
				                        <h3>Final Judging and Presentation</h3>
				                      </div><!-- /timeline-heading -->
				                    </div> <!-- /timeline-content -->
				                  </div><!-- /timeline-panel -->
				                </li>
				            </ul>
					    </div>
					</div>
					<!-- /row -->
					<!-- /.content -->
					
					<!-- .post-meta -->
					<!-- <div class="post-meta">
						Posted by <a href="#">Jhone</a> on <a href="#">dec 24,2014</a>
					</div> -->
					<!-- /.post-meta -->
				</div>
				<!-- /.content-holder -->

			</div>
		</div>
	</div>
</section>
<?php include "view/include/footer.php" ?>
<?php include 'includes/script.php' ?>

</body>
</html>