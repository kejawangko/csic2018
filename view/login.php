<?php include 'view/include/header_login.php'; ?>

<style type="text/css">
    a{
        color:red;
    }
    a:hover { 
    color: #29363E;;
}
</style>


<!-- #single-blog-post -->
<section id="single-blog-post">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 comment-form-section">
                <div class="form-title text-center">
                    <h1>Login</h1>
                </div>
                <div class="comment-form">
                    <form action="<?php echo BASE_URL;?>login" method="post">
                        <ul class="clearfix">
                            <li class="full"><input name="email" type="text" placeholder="Your Email" required></li>
                            <li class="full"><input name="password" type="password" placeholder="Your Password" required>
                            </li>
                            <?php
                                if(isset($_SESSION['not_found']) && $_SESSION['not_found']!=""){
                                    echo "<li class='full' style='margin-top:-3%;margin-bottom:1.5%;margin-left:24.5%;'><text>*".$_SESSION['not_found']."</text></li>";
                                    $_SESSION['not_found'] = "";
                                }
                            ?>
                            <br>
                            <li class="full"><button name="submit" type="submit" class="hvr-bounce-to-right">Login</button></li>
                            <li class="full text-center" style="color:red; font-size: 10pt;margin-top:-2%;float:center;"><a href="<?php echo BASE_URL;?>register">Don't Have Any Account? click here</a> / <a href="<?php echo BASE_URL;?>forgetPassword">Forgot Password? click here</a> </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /#single-blog-post -->

<?php include "view/include/footer.php" ?>
<?php include "includes/script.php" ?>

<script>
    $( document ).ready(function() {
        <?php
            if(isset($_SESSION['register_success']) && $_SESSION['register_success']!= ''){ ?>
                swal({
                  title: 'Register Success',
                  text: 'Your Account Successfuly Registered! Please Login to Your Account',
                  type: 'success',
                  html: true
                });
                <?php $_SESSION['register_success'] = "";
            }
        ?> 
    });
</script>

</body>
</html>