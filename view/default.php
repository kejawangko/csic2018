<?php include 'view/include/header.php'; ?>
<body>
  <!-- #banner -->
  <section id="about-us" class="gradient-overlay">
    <div class="container">
      <div class="row">
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <div class="col-lg-5 col-md-5 col-sm-5">
          <div class="app-box wow slideInLeft">
            <img src="<?php echo BASE_URL;?>template/img/app-section/bgku.png" alt="">
          </div>
        </div>

        <div class="col-lg-7 col-md-7 col-sm-7">
          <div class="section-title">
            <h1 style="color: white;">FESTIVAL UMN 2017</h1>
          </div>

          <p style="color: white;" align="justify">
            FesTIval is an annual event organized by Computer Science of Universitas Multimedia Nusantara. 
            As for the main theme of this event is "ALTERATION",  with hope FesTIval can be present to become a pioneer for the movement of change in the field of technology and social for all participants which channeled in various activities in FesTIval .
            <br><br>
            <br>
            <br>
            <br>
          </p>
          <ul>
            <li class="scrollToLink"><a class="dwnld-now hvr-underline-reveal" href="#upcoming-event" style="color: white;">LEARN MORE...</a></li>
          </ul>
          <br>
          <br>
          <br>
          <br>

        </div>
      </div>
    </div>
  </section>
  <!-- /#banner -->

  <!-- #upcoming-event -->
  <section id="upcoming-event">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 col-md-7 col-sm-7">
          <div class="section-title">
            <h1>Our Events</h1>
            <p align="justify">We are waiting for your participation. Please take a look at our events and join the challenging ones!</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="tab-title-wrap">
            <ul class="clearfix">
              <li class="filter active" data-filter="all"><span>All Event</span></li>
              <li class="filter" data-filter=".coding"><span>Coding</span></li>
              <li class="filter" data-filter=".sport"><span>Sport</span></li>
              <li class="filter" data-filter=".social"><span>Social</span></li>
              <li class="filter" data-filter=".party"><span>Party</span></li>
            </ul>
          </div>
          <div class="tab-content-wrap row">
            <div class="col-lg-3 col-md-4 col-sm-6 mix coding hvr-float-shadow wow fadeIn">
              <div class="img-holder"><img src="<?php echo BASE_URL;?>template/img/upcoming-event/CP-SEN272.jpg" alt=""></div>
              <div class="content-wrap">
                <img src="<?php echo BASE_URL;?>template/img/upcoming-event/45-cp.png" alt="" class="author-img">
                <div class="meta">
                  <ul>
                    <li><span><i class="fa fa-clock-o"></i>31 July, 2017</span></li>
                    <li><span><i class="fa fa-map-marker"></i>UMN</span></li>
                  </ul>
                </div>
                <h3>Senior Competitive Programming Contest</h3>
                <p>Competitive Programming Contest (CPC) aims to test [...]</p>
                <a class="read-more" href="<?php echo BASE_URL;?>cps">read more<i class="fa fa-angle-right"></i></a>
              </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 mix coding hvr-float-shadow wow fadeIn">
              <div class="img-holder"><img src="<?php echo BASE_URL;?>template/img/upcoming-event/CP-JUN272.jpg" alt=""></div>
              <div class="content-wrap">
                <img src="<?php echo BASE_URL;?>template/img/upcoming-event/45-cp.png" alt="" class="author-img">
                <div class="meta">
                  <ul>
                    <li><span><i class="fa fa-clock-o"></i>31 July, 2017</span></li>
                    <li><span><i class="fa fa-map-marker"></i>UMN</span></li>
                  </ul>
                </div>
                <h3>Junior Competitive Programming Contest</h3>
                <p>Competitive Programming Contest (CPC) aims to test [...]</p>
                <a class="read-more" href="<?php echo BASE_URL;?>cpj">read more<i class="fa fa-angle-right"></i></a>
              </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 mix coding hvr-float-shadow wow fadeIn">
              <div class="img-holder"><img src="<?php echo BASE_URL;?>template/img/upcoming-event/CSIC-272.jpg" alt=""></div>
              <div class="content-wrap">
                <img src="<?php echo BASE_URL;?>template/img/upcoming-event/45-csic.png" alt="" class="author-img">
                <div class="meta">
                  <ul>
                    <li><span><i class="fa fa-clock-o"></i>30 Aug, 2017</span></li>
                    <li><span><i class="fa fa-map-marker"></i>UMN</span></li>
                  </ul>
                </div>
                <h3>Computer Science Innovative Challenge</h3>
                <p>Computer Science Innovative Challenge (CSIC) is a creative [...]</p>
                <a class="read-more" href="<?php echo BASE_URL;?>csic">read more<i class="fa fa-angle-right"></i></a>
              </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 mix coding hvr-float-shadow wow fadeIn">
              <div class="img-holder"><img src="<?php echo BASE_URL;?>template/img/upcoming-event/HACK-272.jpg" alt=""></div>
              <div class="content-wrap">
                <img src="<?php echo BASE_URL;?>template/img/upcoming-event/45-hack.png" alt="" class="author-img">
                <div class="meta">
                  <ul>
                    <li><span><i class="fa fa-clock-o"></i>TBA</span></li>
                    <li><span><i class="fa fa-map-marker"></i>UMN</span></li>
                  </ul>
                </div>
                <h3>Hackathon 2.0</h3>
                <p>Hackathon 2.0 denotes the second year of hackathon competition [...] </p>
                <a class="read-more" href="<?php echo BASE_URL;?>hackathon">read more<i class="fa fa-angle-right"></i></a>
              </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 mix sport hvr-float-shadow ">
              <div class="img-holder"><img src="<?php echo BASE_URL;?>template/img/upcoming-event/FUTSAL-272.jpg" alt=""></div>
              <div class="content-wrap">
                <img src="<?php echo BASE_URL;?>template/img/upcoming-event/45-futsal.png" alt="" class="author-img">
                <div class="meta">
                  <ul>
                    <li><span><i class="fa fa-clock-o"></i>TBA</span></li>
                    <li><span><i class="fa fa-map-marker"></i>UMN</span></li>
                  </ul>
                </div>
                <h3>Futsal</h3>
                <p>Not only presenting academic competition, FesTIval also presents Futsal [...]</p>
                <a class="read-more" href="<?php echo BASE_URL;?>futsal">read more<i class="fa fa-angle-right"></i></a>
              </div>
            </div>  
            <div class="col-lg-3 col-md-4 col-sm-6 mix sport hvr-float-shadow ">
              <div class="img-holder"><img src="<?php echo BASE_URL;?>template/img/upcoming-event/BASKET-272.jpg" alt=""></div>
              <div class="content-wrap">
                <img src="<?php echo BASE_URL;?>template/img/upcoming-event/45-basket.png" alt="" class="author-img">
                <div class="meta">
                  <ul>
                    <li><span><i class="fa fa-clock-o"></i>TBA</span></li>
                    <li><span><i class="fa fa-map-marker"></i>UMN</span></li>
                  </ul>
                </div>
                <h3>Basket 3on3</h3>
                <p>FesTIval is coming back this year with 3v3 Basketball competition for students [...]</p>
                <a class="read-more" href="<?php echo BASE_URL;?>basket">read more<i class="fa fa-angle-right"></i></a>
              </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 mix social hvr-float-shadow ">
              <div class="img-holder"><img src="<?php echo BASE_URL;?>template/img/upcoming-event/HOC-272.jpg" alt=""></div>
              <div class="content-wrap">
                <img src="<?php echo BASE_URL;?>template/img/upcoming-event/45-hoc.png" alt="" class="author-img">
                <div class="meta">
                  <ul>
                    <li><span><i class="fa fa-clock-o"></i>TBA</span></li>
                    <li><span><i class="fa fa-map-marker"></i>Tangerang</span></li>
                  </ul>
                </div>
                <h3>Hour of Code</h3>
                <p>As the world needs all people to have strong logical thinking ability [...]</p>
                <a class="read-more" href="<?php echo BASE_URL;?>hoc">read more<i class="fa fa-angle-right"></i></a>
              </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 mix party hvr-float-shadow ">
              <div class="img-holder"><img src="<?php echo BASE_URL;?>template/img/upcoming-event/TIGATH-272.jpg" alt=""></div>
              <div class="content-wrap">
                <img src="<?php echo BASE_URL;?>template/img/upcoming-event/45-tigath.png" alt="" class="author-img">
                <div class="meta">
                  <ul>
                    <li><span><i class="fa fa-clock-o"></i>TBA</span></li>
                    <li><span><i class="fa fa-map-marker"></i>UMN</span></li>
                  </ul>
                </div>
                <h3>TI Gathering</h3>
                <p>TI Gathering returns as the closing event of FesTIval 2017. TI Gathering is [...]</p>
                <a class="read-more" href="<?php echo BASE_URL;?>tigath">read more<i class="fa fa-angle-right"></i></a>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /#upcoming-event -->

  <!-- #event-speakers -->
  <!-- <section id="event-speakers" class="gradient-overlay"> -->
    <!-- <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-title text-center">
            <h1 style="color: white;">EVENT SPEAKERS</h1>
            <p style="color: white;">Lorem ipsum dolor sit amet.  </p>
        
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-md-8 col-sm-8 col-lg-offset-0 col-md-offset-1 col-sm-offset-1">
      
          <div class="single-speakers row">
            <div class="info text-right col-lg-6 col-md-7 col-sm-7">
              <h3 style="color: white;">Hendro Wijaya</h3>
              <span class="position" style="color: white;">Front End Web Developer</span>
              <p style="color: white;">My name is Hendro Wijaya</p>
              <ul class="social">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
            <div class="img-holder text-center col-lg-6 col-md-5 col-sm-5">
              <div class="img-container"><img src="<?php echo BASE_URL;?>template/img/speakers/1.png" alt=""></div>
            </div>
          </div>
        
          <div class="single-speakers row">
            <div class="info text-right col-lg-6 col-md-7 col-sm-7">
              <h3 style="color: white;">Hermawan</h3>
              <span class="position" style="color: white;">UI/UX Designer</span>
              <p style="color: white;">My name is Hermawan</p>
              <ul class="social">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
            <div class="img-holder text-center col-lg-6 col-md-5 col-sm-5">
              <div class="img-container"><img src="<?php echo BASE_URL;?>template/img/speakers/2.png" alt=""></div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-8 col-sm-10 col-lg-offset-0 col-md-offset-1 col-sm-offset-1">
          
          <div class="single-speakers row">
            <div class="img-holder text-center col-lg-6 col-md-5 col-sm-5">
              <div class="img-container" data-wow-delay=".6s"><img src="<?php echo BASE_URL;?>template/img/speakers/3.png" alt=""></div>
            </div>
            <div class="info text-left col-lg-6 col-md-7 col-sm-7">
              <h3 style="color: white;">Keshia Tiffany Wangko</h3>
              <span class="position" style="color: white;">Front End Web Developer</span>
              <p style="color: white;">My name is Keshia Tiffany Wangko</p>
              <ul class="social">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="single-speakers row">
            <div class="img-holder text-center col-lg-6 col-md-5 col-sm-5">
              <div class="img-container"><img src="<?php echo BASE_URL;?>template/img/speakers/4.png" alt=""></div>
            </div>
            <div class="info text-left col-lg-6 col-md-7 col-sm-7">
              <h3 style="color: white;">Sintya Oktaviani</h3>
              <span class="position" style="color: white;">Back End Web Developer</span>
              <p style="color: white;">My name is Sintya Oktaviani</p>
              <ul class="social">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div> -->
  <!-- </section> -->
  <!-- /#event-speakers -->

  <!-- #gallery -->
  <section id="gallery">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-title text-center">
            <h1>Our Gallery</h1>
            <p>Just a sneak peek of our last event.</p>
          </div>
        </div>
      </div>
      <div class="row gallery-image">
        <a class="fancybox" href="<?php echo BASE_URL;?>template/img/gallery/1-high.jpg"><div class="col-lg-3 col-md-4 col-sm-6">
          <img src="<?php echo BASE_URL;?>template/img/gallery/1.jpg" alt="">
        </div></a>
        <a class="fancybox" href="<?php echo BASE_URL;?>template/img/gallery/2-high.jpg"><div class="col-lg-3 col-md-4 col-sm-6">
          <img src="<?php echo BASE_URL;?>template/img/gallery/2.jpg" alt="">
        </div></a>
        <a class="fancybox" href="<?php echo BASE_URL;?>template/img/gallery/3-high.jpg"><div class="col-lg-3 col-md-4 col-sm-6">
          <img src="<?php echo BASE_URL;?>template/img/gallery/3.jpg" alt="">
        </div></a>
        <a class="fancybox" href="<?php echo BASE_URL;?>template/img/gallery/4-high.jpg"><div class="col-lg-3 col-md-4 col-sm-6">
          <img src="<?php echo BASE_URL;?>template/img/gallery/4.jpg" alt="">
        </div></a>
        <a class="fancybox" href="<?php echo BASE_URL;?>template/img/gallery/5-high.jpg"><div class="col-lg-3 col-md-4 col-sm-6">
          <img src="<?php echo BASE_URL;?>template/img/gallery/5.jpg" alt="">
        </div></a>
        <a class="fancybox" href="<?php echo BASE_URL;?>template/img/gallery/6-high.jpg"><div class="col-lg-3 col-md-4 col-sm-6">
          <img src="<?php echo BASE_URL;?>template/img/gallery/6.jpg" alt="">
        </div></a>
        <a class="fancybox" href="<?php echo BASE_URL;?>template/img/gallery/7-high.jpg"><div class="col-lg-3 col-md-4 col-sm-6">
          <img src="<?php echo BASE_URL;?>template/img/gallery/7.jpg" alt="">
        </div></a>
        <a class="fancybox" href="<?php echo BASE_URL;?>gallery"><div class="col-lg-3 col-md-4 col-sm-6">
          <img src="<?php echo BASE_URL;?>template/img/gallery/8.png" alt="">
        </div></a>
      </div>
    </div>
  </section>
  <!-- /#gallery -->

  <!-- #event-sponsor -->
  <section id="event-sponsor">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-title">
            <h1>EVENT SPONSOR</h1>
            <!-- <p>Thank you for sponsor and media partner.</p> -->
            <!-- <hr> -->
          </div>
        </div>
      </div>
      <div class="row sponsor-logo-row">
        <div class="col-lg-12">
          <div class="item col-lg-4">
            <a href="http://www.phi-integration.com/" target="_blank" class="hvr-underline-reveal"><img src="<?php echo BASE_URL;?>template/img/sponsor-logo/phi.png" alt=""></a>
          </div>
        </div>
      </div>
      <div class="row sponsor-logo-row">
        <div class="col-lg-12">
          <div class="item col-lg-3" >
            <a href="https://www.dellemc.com/id-id/index.htm/" target="_blank" class="hvr-underline-reveal"><img src="<?php echo BASE_URL;?>template/img/sponsor-logo/dell.png" alt=""></a>
          </div>
          <div class="item col-lg-3">
            <a href="https://www.synnexmetrodata.com/" target="_blank" class="hvr-underline-reveal"><img src="<?php echo BASE_URL;?>template/img/sponsor-logo/synex.png" alt=""></a>
          </div>
          
        </div>
      </div>
      <hr>
      <br>
      <br>
      <div class="row">
        <div class="col-lg-12">
          <div class="section-title">
            <h1>EVENT MEDIA PARTNER</h1>
            <!-- <p>Thank you for sponsor and media partner.</p> -->
            <!-- <hr> -->
          </div>
        </div>
      </div>
      <div class="row sponsor-logo-row">
        <div class="col-lg-12">
          <div class="item col-lg-2">
            <img src="<?php echo BASE_URL;?>template/img/medpar/1.png" alt="">
          </div>
          <div class="item col-lg-2" >
            <img src="<?php echo BASE_URL;?>template/img/medpar/2.png" alt="">
          </div>
          <div class="item col-lg-2">
            <img src="<?php echo BASE_URL;?>template/img/medpar/3.png" alt="">
          </div>
          <div class="item col-lg-2">
            <img src="<?php echo BASE_URL;?>template/img/medpar/4.png" alt="">
          </div>
          <div class="item col-lg-2" >
            <img src="<?php echo BASE_URL;?>template/img/medpar/5.png" alt="">
          </div>
          <div class="item col-lg-2" >
            <img src="<?php echo BASE_URL;?>template/img/medpar/6.png" alt="">
          </div>
        </div>
      </div>
      <hr>
    </div>
  </section>
  <!-- /#event-sponsor -->

  <!-- #subscribe-newsletter -->
  <section id="subscribe-newsletter">
    <div class="container">
      <div class="gradient-overlay">
        <div class="row">
          <div class="col-lg-8 col-md-8 col-sm-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-1">
            <div class="text-box col-lg-6 col-md-6 col-sm-6">
              Subscribe Our News
            </div>
            <div class="input-box col-lg-6 col-md-6 col-sm-6">
              <form action="<?php echo BASE_URL;?>subscribe" method="post">
                <input type="email" name="email" placeholder="Enter Email">
                <button type="submit" name="submit"><i class="fa fa-paper-plane"></i></button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /#subscribe-newsletter -->

  <?php include "view/include/footer.php" ?>

  <?php include "includes/script.php" ?>

</body>
</html>