<?php include 'view/include/header_csic_regis.php'; ?>

<style>
  table, td, th {    
    border: 1px solid #bbb;
    text-align: left;
  }

  table {
      border-collapse: collapse;
      width: 100%;
  }

  th, td {
      padding: 15px;
  }
</style>
<?php

if(isset($_SESSION['error']) && !isset($_POST['submit'])){
  echo $_SESSION['error'];
  $_SESSION['error'] = "";
}
else if(isset($_SESSION['register_success']) && !isset($_POST['submit'])){
  $_SESSION['register_success'] = "";
}
?>
<?php
if(isset($_SESSION['csic_team']['payment_proof'])){
  if($_SESSION['csic_team']['payment_proof'] == '-'){
    ?>
    <section id="single-blog-post">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 comment-form-section">
            <p>Please Upload Your Payment Proof. Once you upload your payment proof, you can't change any information related to this team any more.</p>
            <hr>
            <br>
            <div class="comment-form">
              <form action="<?php echo BASE_URL."proofCsic";?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="payment_proof">Upload Payment Proof :</label>
                  <input type="file" name="payment_proof" id="payment_proof">
                  <?php if(isset($_SESSION['payment_proof_error'])  && !isset($_POST['submit'])){ ?>
                  <span class="help-block">
                    <strong><?php echo $_SESSION['payment_proof_error']; ?></strong>
                  </span>
                  <?php
                  $_SESSION['payment_proof_error'] = "";
                } ?>
              </div>
              <button type="submit" name="submit" class="btn btn-default">Upload</button>
            </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  <?php
}
else{ ?>
  <div class="container">
    <div class="row">
      <div class="post-title">
        <h1 style="margin-top:5%;text-transform:uppercase;"><?php echo $_SESSION['csic_team']['title'];?> DETAIL</h1>
      </div>
      <div style="letter-spacing: 3px;font-size: 12pt;margin-top:-1%;margin-bottom:5%;color:blue;font-weight: bold;">Status : <?php if($_SESSION['csic_team']['payment_proof'] == '1' && ($_SESSION['csic_team']['proposal'] == '' || $_SESSION['csic_team']['proposal'] == 'NULL')) echo "Payment Approved"; elseif($_SESSION['csic_team']['payment_proof'] == '1' && $_SESSION['csic_team']['proposal'] != 'NULL' && $_SESSION['csic_team']['proposal'] != '') echo "Payment Approved & Proposal Submited"; else echo "Waiting For Payment Approval"; ?></div>
      <h4><strong>Institution</strong> : <?php echo $_SESSION['csic_team']['institution'];?><br><br>
        <?php if($_SESSION['csic_team']['payment_proof'] == '1'){
        ?></h4>
        <a href="<?php echo BASE_URL;?>storage/Template Proposal CSIC 2018.docx" class="text-center colored hvr-bounce-to-right" id="rulebook" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;margin-top: 1%; margin-bottom: 25px;">
          PROPOSAL TEMPLATE
      </a>
      <?php if($_SESSION['csic_team']['payment_proof'] == '1'  && ($_SESSION['csic_team']['proposal'] == 'NULL'  || $_SESSION['csic_team']['proposal'] == '')) { ?>
        <form action="<?php echo BASE_URL."proposalCsic";?>" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <label for="proposal" style="display:inline">Upload Your Proposal and Prototype's Link :</label><br><span style="color: red;font-size: 9pt;font-weight:bold">*you can only upload your link ONCE</span><br>
            <input style="width: 35%; margin-right: 15px;padding: 5px;" type="text" name="proposal" id="proposal"><button type="submit" name="submit" class="btn btn-default">Upload</button>
          </div>          
        </form>
      <?php } ?>
      <?php
       } ?>
      <table style="margin-bottom:100px;">
        <thead>
          <tr>
            <th></th>
            <th>First Member</th>
            <th>Second Member</th>
            <th>Third Member</th>
            <th>Fourth Member</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Full Name</td>
            <?php 
              for($i = 0; $i<4; $i++){
                echo "<td>";
                if(strlen($_SESSION['csic_member'][$i]['full_name']) == 0){
                  echo "-";
                }
                else echo $_SESSION['csic_member'][$i]['full_name'];
                echo "</td>";
              }
            ?>
          </tr>
          <tr>
            <td>Email</td>
            <?php 
              for($i = 0; $i<4; $i++){
                echo "<td>";
                if(strlen($_SESSION['csic_member'][$i]['email']) == 0){
                  echo "-";
                }
                else echo $_SESSION['csic_member'][$i]['email'];
                echo "</td>";
              }
            ?>
          </tr>
          <tr>
            <td>Phone Number</td>
            <?php 
              for($i = 0; $i<4; $i++){
                echo "<td>";
                if(strlen($_SESSION['csic_member'][$i]['phone']) == 0){
                  echo "-";
                }
                else echo $_SESSION['csic_member'][$i]['phone'];
                echo "</td>";
              }
            ?>
          </tr>
        </tbody>
        
      </table>
    </div>
  </div>
<?php
}
}
?>
<?php
$showForm = 1;
if(isset($_SESSION['csic_team']['payment_proof'])){
  if($_SESSION['csic_team']['payment_proof'] != '-'){
    $showForm = 0;
  }
}
if($showForm == 1){
  ?>
<section id="single-blog-post">
  <div class="container">
    <div class="row" style="margin-top:-5%">
      <div class="col-lg-12 comment-form-section">
        <div class="form-title">
          <h1>Regist Your Team</h1>
          <hr>
        </div>
        <div class="comment-form">
          <form method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label for="skma">Upload requirement files :</label>
              <input type="file" name="skma" id="skma" required>
              <text style="color:green">File must be a zip file max size 10mb</text>
              <!-- <?php if(isset($_SESSION['skma_error']) && !isset($_POST['submit'])){
                                ?>
              <span class="help-block">
                <strong><?php echo $_SESSION['skma_error']; ?></strong>
              </span>
              <?php
              $_SESSION['skma_error'] = "";
              }?> -->
          </div>
          <?php if(isset($_SESSION['skma_error'])){
            if(strlen($_SESSION['skma_error']) > 0 ){
          ?>
            <span style="color: red;">Compressed all requirement such as your student card, and proof of eligibility in .zip format. Max file size: 10MB</span>
            <?php
              $_SESSION['skma_error'] = "";
            } else { ?>
              Compressed all requirement such as your student card, and proof of eligibility in .zip format. Max file size: 10MB
            <?php
            }
          }?>
          <div class="form-group">
            <label for="TeamName">Team Name :</label>
            <?php if(isset($_SESSION['csic_team'])){
              ?>
              <input type="text" class="form-control" id="TeamName" placeholder="Enter your team name" name="TeamName" value="<?php echo $_SESSION['csic_team']['title']; ?>" required>
              <?php
            }
            else { 
              ?>
              <input type="text" class="form-control" id="TeamName" placeholder="Enter your team name" name="TeamName" value="" required>
              <?php
            }
              if(isset($_SESSION['TeamName_error']) && !isset($_POST['submit'])){
            ?>
            <span class="help-block">
                <strong><?php echo $_SESSION['TeamName_error']; ?></strong>
            </span>
            <?php
                $_SESSION['TeamName_error'] = "";
            }?>
      </div>
      <div class="form-group" style="margin-bottom : 5%;">
        <label for="IName">Institution Name :</label>
        <?php if(isset($_SESSION['csic_team'])){
          ?>
          <input type="text" class="form-control" id="IName" placeholder="Enter your institution name" name="IName" value="<?php echo $_SESSION['csic_team']['institution']; ?>" required>
          <?php
        }else{
          ?>
          <input type="text" class="form-control" id="IName" placeholder="Enter your institution name" name="IName" value="" required>
          <?php
        }
          if(isset($_SESSION['IName_error']) && !isset($_POST['submit'])){
        ?>
        <span class="help-block">
            <strong><?php echo $_SESSION['IName_error']; ?></strong>
        </span>
        <?php
            $_SESSION['IName_error'] = "";
        }?>
      </div>
      <div class="form-group">
      <h2 style="margin-left:-5%"> First Member </h2>
        <label for="name1">Full Name :</label>
        <?php if(isset($_SESSION['csic_team'])){
          ?>
          <input type="text" class="form-control" id="name1" placeholder="Enter your name" name="name1" value="<?php echo $_SESSION['csic_member'][0]['full_name']; ?>" required>
          <?php
        }else{
          ?>
          <input type="text" class="form-control" id="name1" placeholder="Enter your name" name="name1" value="" required>
          <?php
        }if(isset($_SESSION['name1_error']) && !isset($_POST['submit'])){
        ?>
        <span class="help-block">
            <strong><?php echo $_SESSION['name1_error']; ?></strong>
        </span>
        <?php
            $_SESSION['name1_error'] = "";
        }?>
      </div>
      <div class="form-group">
        <label for="email1">Email :</label>
        <?php if(isset($_SESSION['csic_team'])){
          ?>
          <input type="email" class="form-control" id="email1" placeholder="Enter your email" name="email1" value="<?php echo $_SESSION['csic_member'][0]['email']; ?>" required>
          <?php
        }else{
          ?>
          <input type="email" class="form-control" id="email1" placeholder="Enter your email" name="email1" value="" required>
          <?php
        }if(isset($_SESSION['email1_error']) && !isset($_POST['submit'])){
        ?>
        <span class="help-block">
            <strong><?php echo $_SESSION['email1_error']; ?></strong>
        </span>
        <?php
            $_SESSION['email1_error'] = "";
        }?>
      </div>
      <div class="form-group">
        <label for="phone1">Phone Number :</label>
        <?php if(isset($_SESSION['csic_team'])){
          ?>
          <input type="text" class="form-control" id="phone1" placeholder="Enter your phone number" name="phone1" value="<?php echo $_SESSION['csic_member'][0]['phone']; ?>" required>
          <?php
        }else{
          ?>
          <input type="text" class="form-control" id="phone1" placeholder="Enter your phone number" name="phone1" value="" required>
          <?php
        } if(isset($_SESSION['phone1_error']) && !isset($_POST['submit'])){
        ?>
        <span class="help-block">
            <strong><?php echo $_SESSION['phone1_error']; ?></strong>
        </span>
        <?php
            $_SESSION['phone1_error'] = "";
        }?>
      </div>
      <div class="form-group">
      <h2 style="margin-left:-5%;margin-top:5%"> Second Member </h2>
        <label for="name2">Full Name :</label>
        <?php if(isset($_SESSION['csic_team'])){
          ?>
          <input type="text" class="form-control" id="name2" placeholder="Enter your name" name="name2" value="<?php echo $_SESSION['csic_member'][1]['full_name']; ?>" required>
          <?php
        }else{
          ?>
          <input type="text" class="form-control" id="name2" placeholder="Enter your name" name="name2" value="" required>
          <?php
        } if(isset($_SESSION['name2_error']) && !isset($_POST['submit'])){
        ?>
        <span class="help-block">
            <strong><?php echo $_SESSION['name2_error']; ?></strong>
        </span>
        <?php
            $_SESSION['name2_error'] = "";
        }?>
      </div>
      <div class="form-group">
        <label for="email2">Email :</label>
        <?php if(isset($_SESSION['csic_team'])){
          ?>
          <input type="email" class="form-control" id="email2" placeholder="Enter your email" name="email2" value="<?php echo $_SESSION['csic_member'][1]['email']; ?>" required>
          <?php
        }else{
          ?>
          <input type="email" class="form-control" id="email2" placeholder="Enter your email" name="email2" value="" required>
          <?php
        } if(isset($_SESSION['email2_error']) && !isset($_POST['submit'])){
        ?>
        <span class="help-block">
            <strong><?php echo $_SESSION['email2_error']; ?></strong>
        </span>
        <?php
            $_SESSION['email2_error'] = "";
        }?>
      </div>
      <div class="form-group">
        <label for="phone2">Phone Number :</label>
        <?php if(isset($_SESSION['csic_team'])){
          ?>
          <input type="text" class="form-control" id="phone2" placeholder="Enter your phone number" name="phone2" value="<?php echo $_SESSION['csic_member'][1]['phone']; ?>" required>
          <?php
        }else{
          ?>
          <input type="text" class="form-control" id="phone2" placeholder="Enter your phone number" name="phone2" value="" required>
          <?php
        }if(isset($_SESSION['phone2_error']) && !isset($_POST['submit'])){
        ?>
        <span class="help-block">
            <strong><?php echo $_SESSION['phone2_error']; ?></strong>
        </span>
        <?php
            $_SESSION['phone2_error'] = "";
        }?>
      </div>
      <div class="form-group">
      <h2 style="margin-left:-5%;margin-top:5%"> Third Member </h2>
        <label for="name3">Full Name :</label>
        <?php if(isset($_SESSION['csic_team'])){
          ?>
          <input type="text" class="form-control" id="name3" placeholder="Enter your name" name="name3" value="<?php echo $_SESSION['csic_member'][2]['full_name']; ?>">
          <?php
        }else{
          ?>
          <input type="text" class="form-control" id="name3" placeholder="Enter your name" name="name3" value="" >
          <?php
        }?>
      </div>
      <div class="form-group">
        <label for="email3">Email :</label>
        <?php if(isset($_SESSION['csic_team'])){
          ?>
          <input type="email" class="form-control" id="email3" placeholder="Enter your email" name="email3" value="<?php echo $_SESSION['csic_member'][2]['email']; ?>" >
          <?php
        }else{
          ?>
          <input type="email" class="form-control" id="email3" placeholder="Enter your email" name="email3" value="" >
          <?php
        }?>
      </div>
      <div class="form-group">
        <label for="phone3">Phone Number :</label>
        <?php if(isset($_SESSION['csic_team'])){
          ?>
          <input type="text" class="form-control" id="phone3" placeholder="Enter your phone number" name="phone3" value="<?php echo $_SESSION['csic_member'][2]['phone']; ?>" >
          <?php
        }else{
          ?>
          <input type="text" class="form-control" id="phone3" placeholder="Enter your phone number" name="phone3" value="" >
          <?php
        }?>
      </div>
      <div class="form-group">
      <h2 style="margin-left:-5%;margin-top:5%"> Fourth Member </h2>
        <label for="name4">Full Name :</label>
        <?php if(isset($_SESSION['csic_team'])){
          ?>
          <input type="text" class="form-control" id="name4" placeholder="Enter your name" name="name4" value="<?php echo $_SESSION['csic_member'][3]['full_name']; ?>" >
          <?php
        }else{
          ?>
          <input type="text" class="form-control" id="name4" placeholder="Enter your name" name="name4" value="" >
          <?php
        }?>
      </div>
      <div class="form-group">
        <label for="email4">Email :</label>
        <?php if(isset($_SESSION['csic_team'])){
          ?>
          <input type="email" class="form-control" id="email4" placeholder="Enter your email" name="email4" value="<?php echo $_SESSION['csic_member'][3]['email']; ?>" >
          <?php
        }else{
          ?>
          <input type="email" class="form-control" id="email4" placeholder="Enter your email" name="email4" value="" >
          <?php
        }?>
      </div>
      <div class="form-group">
        <label for="phone4">Phone Number:</label>
        <?php if(isset($_SESSION['csic_team'])){
          ?>
          <input type="text" class="form-control" id="phone4" placeholder="Enter your phone number" name="phone4" value="<?php echo $_SESSION['csic_member'][3]['phone']; ?>" >
          <?php
        }else{
          ?>
          <input type="text" class="form-control" id="phone4" placeholder="Enter your phone number" name="phone4" value="" >
          <?php
        }?>
      </div>
      <span style="margin-left:-5%;margin-top:5%;color:blue"><strong>Please make sure you have read the rulebook before proceeding.</strong><br></span>
      <ul>
        <li>
            <button name="submit" type="submit" class="btn hvr-bounce-to-right" style="margin-left:-5%;margin-top:2%;width:20%;letter-spacing: 2px;">
              <?php if(isset($_SESSION['csic_team'])) echo "Update Team"; else echo "Create Team";?>
            </button>
        </li>
      </ul>            
    </form>
    <?php 
    }
    ?>
    </div>
  </div>
</div>
</div>
</section>
<!-- /#single-blog-post -->

<?php include "view/include/footer.php" ?>
<?php include "includes/script.php" ?>

<script>
    $( document ).ready(function() {
        <?php
            if(isset($_SESSION['register_team_success']) && $_SESSION['register_team_success']!= ''){ ?>
                swal({
                  title: 'Register Success',
                  text: 'Your Team Successfuly Registered! Please Upload Your Payment Proof.',
                  type: 'success',
                  html: true
                });
                <?php
            }
            $_SESSION['register_team_success'] = '';
        ?>
        <?php
            if(isset($_SESSION['update_success']) && $_SESSION['update_success']!= ''){ ?>
                swal({
                  title: 'Update Success',
                  text: 'Your Team Successfuly Updated! Please Upload Your Payment Proof.',
                  type: 'success',
                  html: true
                });
                <?php
            }
            $_SESSION['update_success'] = '';
        ?>
        <?php
            if(isset($_SESSION['csic_success']) && $_SESSION['csic_success']!= ''){ ?>
                swal({
                  title: 'Update Success',
                  text: 'Your Team Successfuly Registered!<br>Thank You For Participating.<br>Please Wait While Our Team Verify Your Team.',
                  type: 'success',
                  html: true
                });
                <?php
            }
            $_SESSION['csic_success'] = '';
        ?>
        <?php
            if(isset($_SESSION['csic_proposal_success']) && $_SESSION['csic_proposal_success']!= ''){ ?>
                swal({
                  title: 'Update Success',
                  text: 'Your Team Proposal Link Updated Successfuly.',
                  type: 'success',
                  html: true
                });
                <?php
            }
            $_SESSION['csic_proposal_success'] = '';
        ?>
    });
</script>
</body>
</html>