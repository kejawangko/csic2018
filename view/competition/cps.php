<?php include 'view/include/header_cps_regis.php'; ?>

<style>
  table, td, th {    
    border: 1px solid #bbb;
    text-align: left;
  }

  table {
      border-collapse: collapse;
      width: 100%;
  }

  th, td {
      padding: 15px;
  }
</style>
<?php

if(isset($_SESSION['error']) && !isset($_POST['submit'])){
  echo $_SESSION['error'];
  $_SESSION['error'] = "";
}
else if(isset($_SESSION['register_success']) && !isset($_POST['submit'])){
  echo $_SESSION['register_success'];
  $_SESSION['register_success'] = "";
}
?>
<?php
if(isset($_SESSION['cps_team']['payment_proof'])){
  if($_SESSION['cps_team']['payment_proof'] == '-'){
    ?>
    <section id="single-blog-post">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 comment-form-section">
            <p>Please Upload Your Payment Proof. Once you upload your payment proof, you can't change any information related to this team any more.</p>
            <hr>
            <br>
            <div class="comment-form">
              <form action="<?php echo BASE_URL."proofcps";?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="payment_proof">Upload Payment Proof :</label>
                  <input type="file" name="payment_proof" id="payment_proof">
                  <?php if(isset($_SESSION['payment_proof_error'])){ ?>
                  <span class="help-block">
                    <strong><?php echo $_SESSION['payment_proof_error']; ?></strong>
                  </span>
                  <?php
                  $_SESSION['payment_proof_error'] = "";
                } ?>
              </div>
              <button type="submit" name="submit" class="btn btn-default">Upload</button>
              
            </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  <?php
}
else{ ?>
  <div class="container">
    <div class="row">
      <div class="post-title">
        <h1 style="margin-top:5%;text-transform:uppercase;"><?php echo $_SESSION['cps_team']['title'];?> DETAIL</h1>
      </div>
      <div style="letter-spacing: 3px;font-size: 12pt;margin-top:-1%;margin-bottom:5%;color:blue;font-weight: bold;">Status : <?php if($_SESSION['cps_team']['payment_proof'] == '1') echo "Payment Approved"; else echo "Waiting For Payment Approval"; ?></div>

      <h4><strong>Institution</strong> : <?php echo $_SESSION['cps_team']['institution'];?></h4>
      <table style="margin-bottom:100px;">
        <thead>
          <tr>
            <th></th>
            <th>First Member</th>
            <th>Second Member</th>
            <th>Third Member</th>
            <th>Coach</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Full Name</td>
            <?php 
              for($i = 0; $i<4; $i++){
                echo "<td>";
                if(strlen($_SESSION['cps_member'][$i]['full_name']) == 0){
                  echo "-";
                }
                else echo $_SESSION['cps_member'][$i]['full_name'];
                echo "</td>";
              }
            ?>
          </tr>
          <tr>
            <td>Email</td>
            <?php 
              for($i = 0; $i<4; $i++){
                echo "<td>";
                if(strlen($_SESSION['cps_member'][$i]['email']) == 0){
                  echo "-";
                }
                else echo $_SESSION['cps_member'][$i]['email'];
                echo "</td>";
              }
            ?>
          </tr>
          <tr>
            <td>Phone Number</td>
            <?php 
              for($i = 0; $i<4; $i++){
                echo "<td>";
                if(strlen($_SESSION['cps_member'][$i]['phone']) == 0){
                  echo "-";
                }
                else echo $_SESSION['cps_member'][$i]['phone'];
                echo "</td>";
              }
            ?>
          </tr>
          <tr>
            <td>Cloth Size</td>
            <?php 
              for($i = 0; $i<4; $i++){
                echo "<td>";
                if(strlen($_SESSION['cps_member'][$i]['full_name']) == 0){
                  echo "-";
                }
                else echo $_SESSION['cps_member'][$i]['cloth_size'];
                echo "</td>";
              }
            ?>
          </tr>
        </tbody>
        
      </table>
    </div>
  </div>
<?php
}
}
?>
<?php
$showForm = 1;
if(isset($_SESSION['cps_team']['payment_proof'])){
  if($_SESSION['cps_team']['payment_proof'] != '-'){
    $showForm = 0;
  }
}
if($showForm == 1){
  ?>
<section id="single-blog-post">
  <div class="container">
    <div class="row" style="margin-top:-5%">
      <div class="col-lg-12 comment-form-section">
        <div class="form-title">
          <h1><?php if(isset($_SESSION['cps_team'])) echo "Update Your Team"; else echo "Register Your Team Here!";?> </h1>
          <hr>
        </div>
        <div class="comment-form">
          <form action="" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label for="skma">Upload files :</label>
              <input type="file" name="skma" id="skma" required>
              <text style="color:green">File must be a zip file max size 10mb</text>
        <!-- @if ($errors->has('skma'))
            <span class="help-block">
                <strong>{{ $errors->first('skma') }}</strong>
            </span>
            @endif -->
          </div>
          <?php if(isset($_SESSION['skma_error'])){
            if(strlen($_SESSION['skma_error']) > 0 ){
          ?>
            <span style="color: red;">Compressed all requirement such as your photo, student card, and proof of eligibility in .zip format. Max file size: 10MB</span>
            <?php
              $_SESSION['skma_error'] = "";
            } else { ?>
              Compressed all requirement such as your photo, student card, and proof of eligibility in .zip format. Max file size: 10MB
            <?php
            }
          }?>
          <div class="form-group">
            <label for="TeamName">Team Name :</label>
            <?php if(isset($_SESSION['cps_team'])){
              ?>
              <input type="text" class="form-control" id="TeamName" placeholder="Enter your team name" name="TeamName" value="<?php echo $_SESSION['cps_team']['title']; ?>" required>
              <?php
            }
            else { 
              ?>
              <input type="text" class="form-control" id="TeamName" placeholder="Enter your team name" name="TeamName" value="" required>
              <?php
            }?>
    <!-- @if ($errors->has('TeamName'))
        <span class="help-block">
            <strong>{{ $errors->first('TeamName') }}</strong>
        </span>
        @endif -->
      </div>
      <div class="form-group" style="margin-bottom : 5%;">
        <label for="IName">Institution Name :</label>
        <?php if(isset($_SESSION['cps_team'])){
          ?>
          <input type="text" class="form-control" id="IName" placeholder="Enter your institution name" name="IName" value="<?php echo $_SESSION['cps_team']['institution']; ?>" required>
          <?php
        }else{
          ?>
          <input type="text" class="form-control" id="IName" placeholder="Enter your institution name" name="IName" value="" required>
          <?php
        }?>
    <!-- @if ($errors->has('IName'))
        <span class="help-block">
            <strong>{{ $errors->first('IName') }}</strong>
        </span>
        @endif -->
      </div>
      <div class="form-group">
      <h2 style="margin-left:-5%"> First Member </h2>
        <label for="name1">Full Name :</label>
        <?php if(isset($_SESSION['cps_team'])){
          ?>
          <input type="text" class="form-control" id="name1" placeholder="Enter your name" name="name1" value="<?php echo $_SESSION['cps_member'][0]['full_name']; ?>" required>
          <?php
        }else{
          ?>
          <input type="text" class="form-control" id="name1" placeholder="Enter your name" name="name1" value="" required>
          <?php
        }?>
    <!-- @if ($errors->has('name1'))
        <span class="help-block">
            <strong>{{ $errors->first('name1') }}</strong>
        </span>
        @endif -->
      </div>
      <div class="form-group">
        <label for="email1">Email :</label>
        <?php if(isset($_SESSION['cps_team'])){
          ?>
          <input type="email" class="form-control" id="email1" placeholder="Enter your email" name="email1" value="<?php echo $_SESSION['cps_member'][0]['email']; ?>" required>
          <?php
        }else{
          ?>
          <input type="email" class="form-control" id="email1" placeholder="Enter your email" name="email1" value="" required>
          <?php
        }?>
    <!-- @if ($errors->has('email1'))
        <span class="help-block">
            <strong>{{ $errors->first('email1') }}</strong>
        </span>
        @endif -->
      </div>
      <div class="form-group">
        <label for="phone1">Phone Number :</label>
        <?php if(isset($_SESSION['cps_team'])){
          ?>
          <input type="text" class="form-control" id="phone1" placeholder="Enter your phone number" name="phone1" value="<?php echo $_SESSION['cps_member'][0]['phone']; ?>" required>
          <?php
        }else{
          ?>
          <input type="text" class="form-control" id="phone1" placeholder="Enter your phone number" name="phone1" value="" required>
          <?php
        }?>
        <div class="form-group">
          <label for="size1">Cloth Size :</label>
          <select name="size1" class="form-control" required style="width: 30%;">
            <option <?php if(isset($_SESSION['cps_member'][0]['cloth_size']) && $_SESSION['cps_member'][0]['cloth_size'] == 'S') echo 'selected';?> value="S">S</option>
            <option <?php if(isset($_SESSION['cps_member'][0]['cloth_size']) && $_SESSION['cps_member'][0]['cloth_size'] == 'M') echo 'selected';?> value="M">M</option>
            <option <?php if(isset($_SESSION['cps_member'][0]['cloth_size']) && $_SESSION['cps_member'][0]['cloth_size'] == 'L') echo 'selected';?> value="L">L</option>
            <option <?php if(isset($_SESSION['cps_member'][0]['cloth_size']) && $_SESSION['cps_member'][0]['cloth_size'] == 'Xl') echo 'selected';?> value="XL">XL</option>
          </select>
    <!-- @if ($errors->has('phone4'))
        <span class="help-block">
            <strong>{{ $errors->first('phone4') }}</strong>
        </span>
        @endif -->
      </div>
    <!-- @if ($errors->has('phone1'))
        <span class="help-block">
            <strong>{{ $errors->first('phone1') }}</strong>
        </span>
        @endif -->
      </div>
      <div class="form-group">
      <h2 style="margin-left:-5%;margin-top:5%"> Second Member </h2>
        <label for="name2">Full Name :</label>
        <?php if(isset($_SESSION['cps_team'])){
          ?>
          <input type="text" class="form-control" id="name2" placeholder="Enter your name" name="name2" value="<?php echo $_SESSION['cps_member'][1]['full_name']; ?>" required>
          <?php
        }else{
          ?>
          <input type="text" class="form-control" id="name2" placeholder="Enter your name" name="name2" value="" required>
          <?php
        }?>
    <!-- @if ($errors->has('name2'))
        <span class="help-block">
            <strong>{{ $errors->first('name2') }}</strong>
        </span>
        @endif -->
      </div>
      <div class="form-group">
        <label for="email2">Email :</label>
        <?php if(isset($_SESSION['cps_team'])){
          ?>
          <input type="email" class="form-control" id="email2" placeholder="Enter your email" name="email2" value="<?php echo $_SESSION['cps_member'][1]['email']; ?>" required>
          <?php
        }else{
          ?>
          <input type="email" class="form-control" id="email2" placeholder="Enter your email" name="email2" value="" required>
          <?php
        }?>
    <!-- @if ($errors->has('email2'))
        <span class="help-block">
            <strong>{{ $errors->first('email2') }}</strong>
        </span>
        @endif -->
      </div>
      <div class="form-group">
        <label for="phone2">Phone Number :</label>
        <?php if(isset($_SESSION['cps_team'])){
          ?>
          <input type="text" class="form-control" id="phone2" placeholder="Enter your phone number" name="phone2" value="<?php echo $_SESSION['cps_member'][1]['phone']; ?>" required>
          <?php
        }else{
          ?>
          <input type="text" class="form-control" id="phone2" placeholder="Enter your phone number" name="phone2" value="" required>
          <?php
        }?>
      </div>
      <div class="form-group">
        <label for="size2">Cloth Size :</label>
        <select name="size2" class="form-control" required style="width: 30%;">
          <option <?php if(isset($_SESSION['cps_member'][1]['cloth_size']) && $_SESSION['cps_member'][1]['cloth_size'] == 'S') echo 'selected';?> value="S">S</option>
          <option <?php if(isset($_SESSION['cps_member'][1]['cloth_size']) && $_SESSION['cps_member'][1]['cloth_size'] == 'M') echo 'selected';?> value="M">M</option>
          <option <?php if(isset($_SESSION['cps_member'][1]['cloth_size']) && $_SESSION['cps_member'][1]['cloth_size'] == 'L') echo 'selected';?> value="L">L</option>
          <option <?php if(isset($_SESSION['cps_member'][1]['cloth_size']) && $_SESSION['cps_member'][1]['cloth_size'] == 'Xl') echo 'selected';?> value="XL">XL</option>
        </select>
    <!-- @if ($errors->has('phone4'))
        <span class="help-block">
            <strong>{{ $errors->first('phone4') }}</strong>
        </span>
        @endif -->
      </div>
      <div class="form-group">
      <h2 style="margin-left:-5%;margin-top:5%"> Third Member </h2>
        <label for="name3">Full Name :</label>
        <?php if(isset($_SESSION['cps_team'])){
          ?>
          <input type="text" class="form-control" id="name3" placeholder="Enter your name" name="name3" value="<?php echo $_SESSION['cps_member'][2]['full_name']; ?>" required>
          <?php
        }else{
          ?>
          <input type="text" class="form-control" id="name3" placeholder="Enter your name" name="name3" value="" required>
          <?php
        }?>
    <!-- @if ($errors->has('name3'))
        <span class="help-block">
            <strong>{{ $errors->first('name3') }}</strong>
        </span>
        @endif -->
      </div>
      <div class="form-group">
        <label for="email3">Email :</label>
        <?php if(isset($_SESSION['cps_team'])){
          ?>
          <input type="email" class="form-control" id="email3" placeholder="Enter your email" name="email3" value="<?php echo $_SESSION['cps_member'][2]['email']; ?>" required>
          <?php
        }else{
          ?>
          <input type="email" class="form-control" id="email3" placeholder="Enter your email" name="email3" value="" required>
          <?php
        }?>
    <!-- @if ($errors->has('email3'))
        <span class="help-block">
            <strong>{{ $errors->first('email3') }}</strong>
        </span>
        @endif -->
      </div>
      <div class="form-group">
        <label for="phone3">Phone Number :</label>
        <?php if(isset($_SESSION['cps_team'])){
          ?>
          <input type="text" class="form-control" id="phone3" placeholder="Enter your phone number" name="phone3" value="<?php echo $_SESSION['cps_member'][2]['phone']; ?>" required>
          <?php
        }else{
          ?>
          <input type="text" class="form-control" id="phone3" placeholder="Enter your phone number" name="phone3" value="" required>
          <?php
        }?>
      </div>
      <div class="form-group">
        <label for="size3">Cloth Size :</label>
        <select name="size3" class="form-control" required style="width: 30%;">
          <option <?php if(isset($_SESSION['cps_member'][2]['cloth_size']) && $_SESSION['cps_member'][2]['cloth_size'] == 'S') echo 'selected';?> value="S">S</option>
          <option <?php if(isset($_SESSION['cps_member'][2]['cloth_size']) && $_SESSION['cps_member'][2]['cloth_size'] == 'M') echo 'selected';?> value="M">M</option>
          <option <?php if(isset($_SESSION['cps_member'][2]['cloth_size']) && $_SESSION['cps_member'][2]['cloth_size'] == 'L') echo 'selected';?> value="L">L</option>
          <option <?php if(isset($_SESSION['cps_member'][2]['cloth_size']) && $_SESSION['cps_member'][2]['cloth_size'] == 'Xl') echo 'selected';?> value="XL">XL</option>
        </select>
    <!-- @if ($errors->has('phone4'))
        <span class="help-block">
            <strong>{{ $errors->first('phone4') }}</strong>
        </span>
        @endif -->
      </div>
      <div class="form-group">
      <h2 style="margin-left:-5%;margin-top:5%"> Coach </h2>
        <label for="name4">Full Name :</label>
        <?php if(isset($_SESSION['cps_team'])){
          ?>
          <input type="text" class="form-control" id="name4" placeholder="Enter your name" name="name4" value="<?php echo $_SESSION['cps_member'][3]['full_name']; ?>" >
          <?php
        }else{
          ?>
          <input type="text" class="form-control" id="name4" placeholder="Enter your name" name="name4" value="" >
          <?php
        }?>
    <!-- @if ($errors->has('name4'))
        <span class="help-block">
            <strong>{{ $errors->first('name4') }}</strong>
        </span>
        @endif -->
      </div>
      <div class="form-group">
        <label for="email4">Coach Email :</label>
        <?php if(isset($_SESSION['cps_team'])){
          ?>
          <input type="email" class="form-control" id="email4" placeholder="Enter your email" name="email4" value="<?php echo $_SESSION['cps_member'][3]['email']; ?>" >
          <?php
        }else{
          ?>
          <input type="email" class="form-control" id="email4" placeholder="Enter your email" name="email4" value="" >
          <?php
        }?>
    <!-- @if ($errors->has('email4'))
        <span class="help-block">
            <strong>{{ $errors->first('email4') }}</strong>
        </span>
        @endif -->
      </div>
      <div class="form-group">
        <label for="phone4">Coach phone number:</label>
        <?php if(isset($_SESSION['cps_team'])){
          ?>
          <input type="text" class="form-control" id="phone4" placeholder="Enter your phone number" name="phone4" value="<?php echo $_SESSION['cps_member'][3]['phone']; ?>" >
          <?php
        }else{
          ?>
          <input type="text" class="form-control" id="phone4" placeholder="Enter your phone number" name="phone4" value="" >
          <?php
        }?>
      </div>
      <div class="form-group">
        <label for="size4">Coach Cloth Size :</label>
        <select name="size4" class="form-control" required style="width: 30%;">
          <option <?php if(isset($_SESSION['cps_member'][3]['cloth_size']) && $_SESSION['cps_member'][3]['cloth_size'] == 'S') echo 'selected';?> value="S">S</option>
          <option <?php if(isset($_SESSION['cps_member'][3]['cloth_size']) && $_SESSION['cps_member'][3]['cloth_size'] == 'M') echo 'selected';?> value="M">M</option>
          <option <?php if(isset($_SESSION['cps_member'][3]['cloth_size']) && $_SESSION['cps_member'][3]['cloth_size'] == 'L') echo 'selected';?> value="L">L</option>
          <option <?php if(isset($_SESSION['cps_member'][3]['cloth_size']) && $_SESSION['cps_member'][3]['cloth_size'] == 'Xl') echo 'selected';?> value="XL">XL</option>
        </select>
    <!-- @if ($errors->has('phone4'))
        <span class="help-block">
            <strong>{{ $errors->first('phone4') }}</strong>
        </span>
        @endif -->
      </div>
      <ul>
              <li>
                  <span>Please make sure you have read the rulebook before proceeding.<br></span>
                  <button disabled name="submit" type="submit" class="btn hvr-bounce-to-right" style="margin-left:-5%;margin-top:2%;width:20%;letter-spacing: 2px;">
                    <?php if(isset($_SESSION['cps_team'])) echo "Update Team"; else echo "Create Team";?>
                  </button>
              </li>
            </ul>            
          </form>
          <?php 
          }
          ?>
    </div>
  </div>
</div>
</div>
</section>
<!-- /#single-blog-post -->

<?php include "view/include/footer.php" ?>
<?php include "includes/script.php" ?>
</body>
</html>