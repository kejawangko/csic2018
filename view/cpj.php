<?php
  include 'view/include/header_cpj.php';
?>
<!-- #page-title -->

<!-- /#page-title -->


<!-- #single-blog-post -->
<section id="single-blog-post">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<!-- .content-holder -->
				<div class="content-holder">
					<!-- .img-holder -->
					<div class="img-holder">
						<img src="<?php echo BASE_URL;?>template/img/cpj.png" alt="" style="width:50%;">
					</div>
					<!-- /.img-holder -->
					<!-- .post-title -->
					<div class="post-title">
						<h1>Junior Competitive Programming</h1>
					</div>
					<!-- /.post-title -->

					<!-- .content -->
					<div class="content">
						<p align="justify">Competitive Programming Contest (CPC) aims to test your skill to solve well-known computer science problems in memory and time constraints. CPC has 2 categories, namely Junior category for high school students and Senior category for undergraduate students (Bachelor degree). This competition is held by FesTIval 2017 in cooperation with UMN Programming Club and now opened to all participants in Indonesia.</p>

					</div>
					
					<!-- /.content -->

					<div style="margin-top:5%;">
						<ul>
							<li>
							<?php if(!isset($_SESSION['cpj_team']['payment_proof'])){ ?>
							<a href="<?php echo BASE_URL;?>cp/registerjcpc" class="colored hvr-bounce-to-right" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;"> REGISTER NOW </a>
							<?php } else if(isset($_SESSION['cpj_team']['payment_proof'])) {?>
							<a href="<?php echo BASE_URL;?>cp/registerjcpc" class="colored hvr-bounce-to-right" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;"> VIEW MY TEAM </a>
							<?php }?>
							</li>
							<li>
								<a href="<?php echo BASE_URL;?>storage/Rulebook JCP FesTIval.pdf" class="colored hvr-bounce-to-right" id="rulebook" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;margin-top: 1%">
									RULEBOOK
								</a>
							</li>
						</ul>
					</div>
				</div>
				<!-- /.content-holder -->

			</div>
		</div>
	</div>
</section>
<!-- /#single-blog-post -->

<!-- #single-blog-post -->
<section id="single-blog-post">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<!-- .content-holder -->
				<div class="content-holder">

					<div class="row">
					    <div class="col-md-12">
					        <div>
					            <h1><b>Timeline</b></h1>
					        </div>
				            <ul class="timeline">
								<li>
				                  <div class="posted-date">
				                    <span class="month">31 July 2017 until 25 August 2017</span>
				                  </div>
				                  <div class="timeline-panel">
				                    <div class="timeline-content">
				                      <div class="timeline-heading">
				                        <h3>Registration</h3>
				                      </div><!-- /timeline-heading -->
				                    </div> <!-- /timeline-content -->
				                  </div><!-- /timeline-panel -->
				                </li>
								<li class="timeline-inverted">
				                  <div class="posted-date">
				                    <span class="month">26 August 2017</span>
				                  </div>
				                  <div class="timeline-panel">
				                    <div class="timeline-content">
				                      <div class="timeline-heading">
				                        <h3>Warming Up</h3>
				                      </div><!-- /timeline-heading -->
				                    </div> <!-- /timeline-content -->
				                  </div><!-- /timeline-panel -->
				                </li>
				                <li>
				                  <div class="posted-date">
				                    <span class="month">27 August 2017</span>
				                  </div>
				                  <div class="timeline-panel">
				                    <div class="timeline-content">
				                      <div class="timeline-heading">
				                        <h3>Preliminary Round</h3>
				                      </div><!-- /timeline-heading -->
				                    </div> <!-- /timeline-content -->
				                  </div><!-- /timeline-panel -->
				                </li>
								<li class="timeline-inverted">
				                  <div class="posted-date">
				                    <span class="month">10 September 2017</span>
				                  </div>
				                  <div class="timeline-panel">
				                    <div class="timeline-content">
				                      <div class="timeline-heading">
				                        <h3>Finalist Announcement</h3>
				                      </div><!-- /timeline-heading -->
				                    </div> <!-- /timeline-content -->
				                  </div><!-- /timeline-panel -->
				                </li>
				                 <li>
				                  <div class="posted-date">
				                    <span class="month">7 October 2017</span>
				                  </div>
				                  <div class="timeline-panel">
				                    <div class="timeline-content">
				                      <div class="timeline-heading">
				                        <h3>Final Round</h3>
				                      </div><!-- /timeline-heading -->
				                    </div> <!-- /timeline-content -->
				                  </div><!-- /timeline-panel -->
				                </li>
				            </ul>
					    </div>
					</div>
					<!-- /row -->
					<!-- /.content -->
					
					<!-- .post-meta -->
					<!-- <div class="post-meta">
						Posted by <a href="#">Jhone</a> on <a href="#">dec 24,2014</a>
					</div> -->
					<!-- /.post-meta -->
				</div>
				<!-- /.content-holder -->

			</div>
		</div>
	</div>
</section>
<!-- /#single-blog-post -->


<!-- #event-sponsor -->
	<section id="event-sponsor">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title">
						<h1>EVENT SPONSOR</h1>
						<!-- <p>Thank you for sponsor and media partner.</p> -->
						<!-- <hr> -->
					</div>
				</div>
			</div>
			<div class="row sponsor-logo-row">
				<div class="col-lg-12">
					<div class="item col-lg-4">
						<a href="http://www.phi-integration.com/" target="_blank" class="hvr-underline-reveal"><img src="<?php echo BASE_URL;?>template/img/sponsor-logo/phi.png" alt=""></a>
					</div>
				</div>
			</div>
			<div class="row sponsor-logo-row">
				<div class="col-lg-12">
					<div class="item col-lg-3" >
						<a href="https://www.dellemc.com/id-id/index.htm/" target="_blank" class="hvr-underline-reveal"><img src="<?php echo BASE_URL;?>template/img/sponsor-logo/dell.png" alt=""></a>
					</div>
					<div class="item col-lg-3">
						<a href="https://www.synnexmetrodata.com/" target="_blank" class="hvr-underline-reveal"><img src="<?php echo BASE_URL;?>template/img/sponsor-logo/synex.png" alt=""></a>
					</div>
					
				</div>
			</div>
			<hr>
		</div>
	</section>
<!-- /#event-sponsor -->
<link rel="stylesheet" type="text/css" href="template/css/style.css">


<?php include "view/include/footer.php" ?>

<?php include 'includes/script.php' ?>


</body>
</html>