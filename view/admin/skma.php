<?php

    include 'includes/link.php';

?>

Pencet ini buat ke <a href="<?php echo BASE_URL;?>home">HOME</a><br>

Pencet ini buat <a href="<?php echo BASE_URL;?>logout">Logout</a><br>



<div class="container">
	<?php if($_SESSION['admin_email'] == 'sintya.oktaviani@student.umn.ac.id' ||
			$_SESSION['admin_email'] == 'hack@fest.umn.ac.id' ){ ?>

	<h2>Hackathon</h2>

	<table border="1" style="width:100%">

		<thead>

			<th style="text-align: center">Team Name</th>

			<th style="text-align: center">Institution</th>

			<th style="text-align: center">Total Member</th>

			<th style="text-align: center">SKMA</th>

			<th style="text-align: center">Payment Link</th>

			<th style="text-align: center">Confirm Payment</th>

			<th style="text-align: center">Proposal</th>

			<th style="text-align: center">Payment Time</th>

			<th style="text-align: center">Proposal Time</th>

		</thead>

		<tbody>

		<?php

			foreach ($_SESSION['hackathon_skma'] as $team){

		?>

			<tr style="text-align: center">

				<td><?php echo $team['title']?></td>

				<td><?php echo $team['institution']?></td>

				<td><?php echo $team['total']?></td>

				<td><a href="<?php echo $team['skma']?>">SKMA</a></td>

				<?php if($team['payment_proof'] == '-'){

				?>

				<td>Not Submitted Yet</td>

				<td>Not Submitted Yet</td>

				<?php

				}?>

				<?php if($team['payment_proof']!= '-' && $team['payment_proof']!= '1'){

				?>

				<td><a href="<?php echo $team['payment_proof']?>">Payment Proof</a></td>

				<td><button class="btn btn-default" name="submit" value="<?php echo $team['id']?>">Approve</button></td>

				<?php

				} else if($team['payment_proof']== '1'){ ?>

				<td>Payment Confirm</td>

				<td><button class="btn btn-success" name="submit" value="">Approve</button></td>

				<?php

				} ?>

				<?php if($team['proposal']!= NULL){

				?>

				<td><a href="<?php echo $team['proposal']?>">Proposal</a></td>

				<?php

				} else{ ?>

				<td>Not Submitted Yet</td>

				<?php

				} ?>

				<td><?php echo $team['payment_time']?></td>

				<td><?php echo $team['proposal_time']?></td>

			</tr>

		<?php

		}?>

		</tbody>

		<tfoot>

			<th style="text-align: center">Team Name</th>

			<th style="text-align: center">Institution</th>

			<th style="text-align: center">Total Member</th>

			<th style="text-align: center">SKMA</th>

			<th style="text-align: center">Payment Link</th>

			<th style="text-align: center">Confirm Payment</th>

			<th style="text-align: center">Proposal</th>

			<th style="text-align: center">Payment Time</th>

			<th style="text-align: center">Proposal Time</th>

		</tfoot>

	</table>

	<?php
	} ?>

	<?php if($_SESSION['admin_email'] == 'sintya.oktaviani@student.umn.ac.id' ||
			$_SESSION['admin_email'] == 'csic@fest.umn.ac.id' ){?>
	<h2>CSIC</h2>

	<table border="1" style="width:100%">

		<thead>
			<th style="text-align: center">No. </th>

			<th style="text-align: center">Team Name</th>

			<th style="text-align: center">Institution</th>

			<th style="text-align: center">Total Member</th>

			<th style="text-align: center">SKMA</th>

			<th style="text-align: center">Payment Link</th>

			<th style="text-align: center">Confirm Payment</th>

			<th style="text-align: center">Proposal</th>

			<th style="text-align: center">Payment Time</th>

			<th style="text-align: center">Proposal Time</th>

		</thead>

		<tbody>

		<?php

			$no = 1;
			foreach ($_SESSION['csic_skma'] as $team){

		?>

			<tr style="text-align: center">
				<td><?php echo $no++; ?></td>

				<td><?php echo $team['title']?></td>

				<td><?php echo $team['institution']?></td>

				<td><?php echo $team['member_names']?> (<?php echo $team['total']?>)</td>

				<td><a href="<?php echo $team['skma']?>">SKMA</a></td>

				<?php if($team['payment_proof']== '-'){

				?>

				<td>Not Submitted Yet</td>

				<td>Not Submitted Yet</td>

				<?php

				}?>

				<?php if($team['payment_proof']!= '-' && $team['payment_proof']!= '1'){

				?>

				<td><a href="<?php echo $team['payment_proof']?>">Payment Proof</a></td>

				<td><button class="btn btn-default" name="submit" value="<?php echo $team['id']?>">Approve</button></td>

				<?php

				} else if($team['payment_proof']== '1'){ ?>

				<td>Payment Confirm</td>

				<td><button class="btn btn-success" name="submit" value="">Approve</button></td>

				<?php

				} ?>

				<?php if($team['proposal']!= NULL){

				?>

				<td><a href="<?php echo $team['proposal']?>">Proposal</a></td>

				<?php

				} else{ ?>

				<td>Not Submitted Yet</td>

				<?php

				} ?>

				<td><?php echo $team['payment_time']?></td>

				<td><?php echo $team['proposal_time']?></td>

			</tr>

		<?php

		}?>

		</tbody>

		<tfoot>

			<th style="text-align: center">No. </th>

			<th style="text-align: center">Team Name</th>

			<th style="text-align: center">Institution</th>

			<th style="text-align: center">Total Member</th>

			<th style="text-align: center">SKMA</th>

			<th style="text-align: center">Payment Link</th>

			<th style="text-align: center">Confirm Payment</th>

			<th style="text-align: center">Proposal</th>

			<th style="text-align: center">Payment Time</th>

			<th style="text-align: center">Proposal Time</th>

		</tfoot>

	</table>

	<?php
	} ?>

	<?php if($_SESSION['admin_email'] == 'sintya.oktaviani@student.umn.ac.id' ||
			$_SESSION['admin_email'] == 'cp@fest.umn.ac.id' ){ ?>

	<h2>Senior Competitive Programming</h2>

	<table border="1" style="width:100%">

		<thead>

			<th style="text-align: center">No. </th>

			<th style="text-align: center">Team Name</th>

			<th style="text-align: center">Institution</th>

			<th style="text-align: center">Total Member</th>

			<th style="text-align: center">SKMA</th>

			<th style="text-align: center">Payment Link</th>

			<th style="text-align: center">Confirm Payment</th>

			<th style="text-align: center">Payment Time</th>

		</thead>

		<tbody>

		<?php

			$no = 1;

			foreach ($_SESSION['scp_skma'] as $team){

		?>

			<tr style="text-align: center">

				<td><?php echo $no++; ?></td>

				<td><?php echo $team['title']?></td>

				<td><?php echo $team['institution']?></td>

				<td><?php echo $team['member_names']?> (<?php echo $team['total']?>)</td>

				<td><a href="<?php echo $team['skma']?>">SKMA</a></td>

				<?php if($team['payment_proof']== '-'){

				?>

				<td>Not Submitted Yet</td>

				<td>Not Submitted Yet</td>

				<?php

				}?>

				<?php if($team['payment_proof']!= '-' && $team['payment_proof']!= '1'){

				?>

				<td><a href="<?php echo $team['payment_proof']?>">Payment Proof</a></td>

				<td><button class="btn btn-default" name="submit" value="<?php echo $team['id']?>">Approve</button></td>

				<?php

				} else if($team['payment_proof']== '1'){ ?>

				<td>Payment Confirm</td>

				<td><button class="btn btn-success" name="submit" value="">Approve</button></td>

				<?php

				} ?>

				<td><?php echo $team['payment_time'];?></td>

			</tr>

		<?php

		}?>

		</tbody>

		<tfoot>

			<th style="text-align: center">No. </th>

			<th style="text-align: center">Team Name</th>

			<th style="text-align: center">Institution</th>

			<th style="text-align: center">Total Member</th>

			<th style="text-align: center">SKMA</th>

			<th style="text-align: center">Payment Link</th>

			<th style="text-align: center">Confirm Payment</th>

			<th style="text-align: center">Payment Time</th>

		</tfoot>

	</table>



	<h2>Junior Competitive Programming</h2>

	<table border="1" style="width:100%">

		<thead>

			<th style="text-align: center">No. </th>

			<th style="text-align: center">Institution</th>

			<th style="text-align: center">Total Member</th>

			<th style="text-align: center">SKMA</th>

			<th style="text-align: center">Payment Link</th>

			<th style="text-align: center">Confirm Payment</th>

			<th style="text-align: center">Payment Time</th>

		</thead>

		<tbody>

		<?php

			$no = 1;

			foreach ($_SESSION['jcp_skma'] as $team){

		?>

			<tr style="text-align: center">

				<td><?php echo $no++; ?></td>

				<td><?php echo $team['institution']?></td>

				<td><?php echo $team['member_names']?> (<?php echo $team['total']?>)</td>

				<td><a href="<?php echo $team['skma']?>">SKMA</a></td>

				<?php if($team['payment_proof']== '-'){

				?>

				<td>Not Submitted Yet</td>

				<td>Not Submitted Yet</td>

				<?php

				}?>

				<?php if($team['payment_proof']!= '-' && $team['payment_proof']!= '1'){

				?>

				<td><a href="<?php echo $team['payment_proof']?>">Payment Proof</a></td>

				<td><button class="btn btn-default" name="submit" value="<?php echo $team['id']?>">Approve</button></td>

				<?php

				} else if($team['payment_proof']== '1'){ ?>

				<td>Payment Confirm</td>

				<td><button class="btn btn-success" name="submit" value="">Approve</button></td>

				<?php

				} ?>

				<td><?php echo $team['payment_time'];?></td>

			</tr>

		<?php

		}?>

		</tbody>

		<tfoot>

			<th style="text-align: center">No. </th>

			<th style="text-align: center">Institution</th>

			<th style="text-align: center">Total Member</th>

			<th style="text-align: center">SKMA</th>

			<th style="text-align: center">Payment Link</th>

			<th style="text-align: center">Confirm Payment</th>

			<th style="text-align: center">Payment Time</th>

		</tfoot>

	</table>
	<?php
	} ?>

</div>



<form id="theForm">

	<input type="hidden" name="team_id" id="team_id">

</form>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>



<!-- Latest compiled JavaScript -->

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>



<script type="text/javascript">

	$( document ).ready(function() {

		$( ".btn-default" ).click(function() {

		  	var id = $(this).val();

			$('#team_id').val(id);



			var data = $('#theForm').serialize();

			console.log("id: " + id)

			$.ajax({

	            url: "<?php echo BASE_URL;?>"+"paymentApprove",

				type: "POST",

	            data: {'data':id},

	            success: function (dataa) {

	                console.log(dataa)

	            },

	            error: function (textStatus, errorThrown) {

	            	console.log('false')

	            }

			})

			location.reload();

		});

	});

</script>