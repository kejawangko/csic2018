<?php include 'view/include/header.php'; ?>

<h1>Payment Proof</h1>

<div class="container">
	<h2>Hackathon</h2>
	<table style="width:100%">
		<thead>
			<th>Team Name</th>
			<th>Institution</th>
			<th>Payment Proof</th>
			<th>Approve</th>
		</thead>
		<tbody>
		<?php
			foreach ($_SESSION['hackathon_payment_proof'] as $team){
		?>
			<?php if($team['payment_proof']!= '-' && $team['payment_proof']!= '1'){
			?>
			<tr>
				<td><?php echo $team['title']?></td>
				<td><?php echo $team['institution']?></td>
				<td><a href="<?php echo $team['payment_proof']?>">Payment Proof</a></td>
				<td><button class="btn btn-default" name="submit" value="<?php echo $team['id']?>">Approve</button></td>
			</tr>
			<?php
			}?>
		<?php
		}?>
		</tbody>
		<tfoot>
			<th>Team Name</th>
			<th>Institution</th>
			<th>Payment Proof</th>
			<th>Approve</th>
		</tfoot>
	</table>

	<h2>CSIC</h2>
	<table style="width:100%">
		<thead>
			<th>Team Name</th>
			<th>Institution</th>
			<th>Payment Proof</th>
			<th>Approve</th>
		</thead>
		<tbody>
		<?php
			foreach ($_SESSION['csic_payment_proof'] as $team){
		?>
			<?php if($team['payment_proof']!= '-' && $team['payment_proof']!= '1'){
			?>
			<tr>
				<td><?php echo $team['title']?></td>
				<td><?php echo $team['institution']?></td>
				<td><a href="<?php echo $team['payment_proof']?>">Payment Proof</a></td>
				<td><button class="btn btn-default" name="submit" value="<?php echo $team['id']?>">Approve</button></td>
			</tr>
			<?php
			}?>
		<?php
		}?>
		</tbody>
		<tfoot>
			<th>Team Name</th>
			<th>Institution</th>
			<th>Payment Proof</th>
			<th>Approve</th>
		</tfoot>
	</table>

	<h2>Junior Competitive Programming</h2>
	<table style="width:100%">
		<thead>
			<th>Team Name</th>
			<th>Institution</th>
			<th>Payment Proof</th>
			<th>Approve</th>
		</thead>
		<tbody>
		<?php
			foreach ($_SESSION['scp_payment_proof'] as $team){
		?>
			<?php if($team['payment_proof']!= '-' && $team['payment_proof']!= '1'){
			?>
			<tr>
				<td><?php echo $team['title']?></td>
				<td><?php echo $team['institution']?></td>
				<td><a href="<?php echo $team['payment_proof']?>">Payment Proof</a></td>
				<td><button class="btn btn-default" name="submit" value="<?php echo $team['id']?>">Approve</button></td>
			</tr>
			<?php
			}?>
		<?php
		}?>
		</tbody>
		<tfoot>
			<th>Team Name</th>
			<th>Institution</th>
			<th>Payment Proof</th>
			<th>Approve</th>
		</tfoot>
	</table>

	<h2>Junior Competitive Programming</h2>
	<table style="width:100%">
		<thead>
			<th>Team Name</th>
			<th>Institution</th>
			<th>Payment Proof</th>
			<th>Approve</th>
		</thead>
		<tbody>
		<?php
			foreach ($_SESSION['jcp_payment_proof'] as $team){
		?>
			<?php if($team['payment_proof']!= '-' && $team['payment_proof']!= '1'){
			?>
			<tr>
				<td><?php echo $team['title']?></td>
				<td><?php echo $team['institution']?></td>
				<td><a href="<?php echo $team['payment_proof']?>">Payment Proof</a></td>
				<td><button class="btn btn-default" name="submit" value="<?php echo $team['id']?>">Approve</button></td>
			</tr>
			<?php
			}?>
		<?php
		}?>
		</tbody>
		<tfoot>
			<th>Team Name</th>
			<th>Institution</th>
			<th>Payment Proof</th>
			<th>Approve</th>
		</tfoot>
	</table>
</div>

<form id="theForm">
	<input type="hidden" name="team_id" id="team_id">
</form>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript">
	$( document ).ready(function() {
		$( ".btn-default" ).click(function() {
		  	var id = $(this).val();
			$('#team_id').val(id);

			var data = $('#theForm').serialize();
			console.log("id: " + id)
			$.ajax({
	            url: "<?php echo BASE_URL;?>"+"paymentApprove",
				type: "POST",
	            data: {'data':id},
	            success: function (dataa) {
	                console.log(dataa)
	            },
	            error: function (textStatus, errorThrown) {
	            	console.log('false')
	            }
			})
			location.reload();
		});
	});
</script>