<?php
    include 'includes/link.php';
?>

<!DOCTYPE html>
<html>
<head>
    <title>Forget Password</title>
</head>
<body>
    <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Forget Password</div>
                <?php if(isset($_SESSION['change_password']) && !isset($_POST['submit'])){
                ?>
                    <strong><?php echo $_SESSION['change_password']; ?></strong>
                <?php
                    session_unset('change_password');
                }?>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="<?php echo BASE_URL;?>changePassword">
                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Old Password</label>
                            <div class="col-md-6">
                                <input id="oldPassword" type="password" class="form-control" name="oldPassword" value="" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="newPassword" class="col-md-4 control-label">New Password</label>
                            <div class="col-md-6">
                                <input id="newPassword" type="password" class="form-control" name="newPassword" value="" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="newPasswordConfirmation" class="col-md-4 control-label">E-Mail Address</label>
                            <div class="col-md-6">
                                <input id="newPasswordConfirmation" type="password" class="form-control" name="newPasswordConfirmation" value="" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" name="submit">
                                    <i class="fa fa-btn fa-user"></i> Change Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>