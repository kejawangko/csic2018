<?php include 'view/include/header.php'; ?>

<h1>Proposal</h1>

<div class="container">
	<h2>Hackathon</h2>
	<table style="width:100%">
		<thead>
			<th>Team Name</th>
			<th>Institution</th>
			<th>proposal</th>
		</thead>
		<tbody>
		<?php
			foreach ($_SESSION['hackathon_proposal'] as $team){
		?>
			<?php if($team['proposal']!= NULL){
			?>
			<tr>
				<td><?php echo $team['title']?></td>
				<td><?php echo $team['institution']?></td>
				<td><a href="<?php echo $team['proposal']?>">proposal</a></td>
			</tr>
			<?php
			}?>
		<?php
		}?>
		</tbody>
		<tfoot>
			<th>Team Name</th>
			<th>Institution</th>
			<th>proposal</th>
		</tfoot>
	</table>

	<h2>CSIC</h2>
	<table style="width:100%">
		<thead>
			<th>Team Name</th>
			<th>Institution</th>
			<th>proposal</th>
		</thead>
		<tbody>
		<?php
			foreach ($_SESSION['csic_proposal'] as $team){
		?>
			<?php if($team['proposal']!= NULL){
			?>
			<tr>
				<td><?php echo $team['title']?></td>
				<td><?php echo $team['institution']?></td>
				<td><a href="<?php echo $team['proposal']?>">proposal</a></td>
			</tr>
			<?php
			}?>
		<?php
		}?>
		</tbody>
		<tfoot>
			<th>Team Name</th>
			<th>Institution</th>
			<th>proposal</th>
		</tfoot>
	</table>

	<h2>Junior Competitive Programming</h2>
	<table style="width:100%">
		<thead>
			<th>Team Name</th>
			<th>Institution</th>
			<th>proposal</th>
		</thead>
		<tbody>
		<?php
			foreach ($_SESSION['scp_proposal'] as $team){
		?>
			<?php if($team['proposal']!= NULL){
			?>
			<tr>
				<td><?php echo $team['title']?></td>
				<td><?php echo $team['institution']?></td>
				<td><a href="<?php echo $team['proposal']?>">proposal</a></td>
			</tr>
			<?php
			}?>
		<?php
		}?>
		</tbody>
		<tfoot>
			<th>Team Name</th>
			<th>Institution</th>
			<th>proposal</th>
		</tfoot>
	</table>

	<h2>Junior Competitive Programming</h2>
	<table style="width:100%">
		<thead>
			<th>Team Name</th>
			<th>Institution</th>
			<th>proposal</th>
		</thead>
		<tbody>
		<?php
			foreach ($_SESSION['jcp_proposal'] as $team){
		?>
			<?php if($team['proposal']!= NULL){
			?>
			<tr>
				<td><?php echo $team['title']?></td>
				<td><?php echo $team['institution']?></td>
				<td><a href="<?php echo $team['proposal']?>">proposal</a></td>
			</tr>
			<?php
			}?>
		<?php
		}?>
		</tbody>
		<tfoot>
			<th>Team Name</th>
			<th>Institution</th>
			<th>proposal</th>
		</tfoot>
	</table>
</div>