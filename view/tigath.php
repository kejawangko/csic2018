<?php
  include 'view/include/header_tigath.php';
?>
<style type="text/css">
	div,
	img,
	ul,
	li,
	section {
	    margin: 0;
	    padding: 0;
	    border: 0;
	    font-weight: inherit;
	    font-style: inherit;
	    font-size: 100%;
	    font-family: inherit;
	    vertical-align: baseline;
	    text-decoration: none;
	    list-style: none;
	}
	img {
	    width: 100%;
	}
	.anim04c {
	    -webkit-transition: all .4s cubic-bezier(.5, .35, .15, 1.4);
	    transition: all .4s cubic-bezier(.5, .35, .15, 1.4);
	}

	html,
	body {
	    width: 100%;
	    height: 100%;
	    font-family: 'Source Sans Pro', sans-serif;
	    background: #eee;
	    color: #666;
	}
	body {
	    overflow-x: hidden;
	    overflow-y: auto;
	}
	/*-----*/

	.outer {
	    position: relative;
	    top: 50%;
	    z-index: 1;
	    -webkit-transform: translateY(-50%);
	    -moz-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    -o-transform: translateY(-50%);
	    transform: translateY(-50%);
	    cursor: pointer;
	}
	/*-----*/

	.signboard {
	    width: 200px;
	    height: 200px;
	    margin: auto;
	    color: #fff;
	    border-radius: 10px;
	}
	/*-----*/

	.front {
	    position: absolute;
	    top: 0;
	    left: 0;
	    z-index: 3;
	    background: #ff726b;
	    text-align: center;
	}
	.right {
	    position: absolute;
	    right: : 0;
	    z-index: 2;
	    -webkit-transform: rotate(-10deg) translate(7px, 8px);
	    -moz-transform: rotate(-10deg) translate(7px, 8px);
	    -ms-transform: rotate(-10deg) translate(7px, 8px);
	    -o-transform: rotate(-10deg) translate(7px, 8px);
	    transform: rotate(-10deg) translate(7px, 8px);
	    background: #EFC94C;
	}
	.left {
	    position: absolute;
	    left: 0;
	    z-index: 1;
	    -webkit-transform: rotate(5deg) translate(-4px, 4px);
	    -moz-transform: rotate(5deg) translate(-4px, 4px);
	    -ms-transform: rotate(5deg) translate(-4px, 4px);
	    -o-transform: rotate(5deg) translate(-4px, 4px);
	    transform: rotate(5deg) translate(-4px, 4px);
	    background: #3498DB;
	}
	/*-----*/

	.outer:hover .inner {
	    -webkit-transform: rotate(0) translate(0);
	    -moz-transform: rotate(0) translate(0);
	    -ms-transform: rotate(0) translate(0);
	    -o-transform: rotate(0) translate(0);
	    transform: rotate(0) translate(0);
	}
	/*-----*/

	.outer:active .inner {
	    -webkit-transform: rotate(0) translate(0) scale(0.9);
	    -moz-transform: rotate(0) translate(0) scale(0.9);
	    -ms-transform: rotate(0) translate(0) scale(0.9);
	    -o-transform: rotate(0) translate(0) scale(0.9);
	    transform: rotate(0) translate(0) scale(0.9);
	}
	.outer:active .front .date {
	    -webkit-transform: scale(2);
	}
	.outer:active .front .day,
	.outer:active .front .month {
	    visibility: hidden;
	    opacity: 0;
	    -webkit-transform: scale(0);
	    -moz-transform: scale(0);
	    -ms-transform: scale(0);
	    -o-transform: scale(0);
	    transform: scale(0);
	}
	.outer:active .right {
	    -webkit-transform: rotate(-5deg) translateX(80px) scale(0.9);
	    -moz-transform: rotate(-5deg) translateX(80px) scale(0.9);
	    -ms-transform: rotate(-5deg) translateX(80px) scale(0.9);
	    -o-transform: rotate(-5deg) translateX(80px) scale(0.9);
	    transform: rotate(-5deg) translateX(80px) scale(0.9);
	}
	.outer:active .left {
	    -webkit-transform: rotate(5deg) translateX(-80px) scale(0.9);
	    -moz-transform: rotate(5deg) translateX(-80px) scale(0.9);
	    -ms-transform: rotate(5deg) translateX(-80px) scale(0.9);
	    -o-transform: rotate(5deg) translateX(-80px) scale(0.9);
	    transform: rotate(5deg) translateX(-80px) scale(0.9);
	}
	/*-----*/

	.outer:active .calendarMain {
	    -webkit-transform: scale(1.8);
	    opacity: 0;
	    visibility: hidden;
	}
	.outer:active .clock {
	    -webkit-transform: scale(1.4);
	    opacity: 1;
	    visibility: visible;
	}
	.outer:active .calendarNormal {
	    bottom: -30px;
	    opacity: 1;
	    visibility: visible;
	}
	.outer:active .year {
	    top: -30px;
	    opacity: 1;
	    visibility: visible;
	    letter-spacing: 3px;
	}
	/*-----*/

	.calendarMain {
	    width: 100%;
	    height: 100%;
	    position: absolute;
	    opacity: 1;
	}
	.month,
	.day {
	    font-size: 20px;
	    line-height: 30px;
	    font-weight: 600;
	    text-transform: uppercase;
	    letter-spacing: 3px;
	}
	.date {
	    font-size: 65px;
	    line-height: 40px;
	    font-weight: 300;
	    text-transform: uppercase;
	    letter-spacing: 3px;
	}
	/*-----*/

	.clock {
	    width: 100%;
	    height: 100%;
	    position: absolute;
	    font-size: 40px;
	    line-height: 100px;
	    font-weight: 300;
	    text-transform: uppercase;
	    letter-spacing: 3px;
	    text-align: center;
	    opacity: 0;
	    visibility: hidden;
	}
	/*-----*/

	.year {
	    width: 100%;
	    position: absolute;
	    top: 0;
	    font-size: 14px;
	    line-height: 30px;
	    font-weight: 300;
	    text-transform: uppercase;
	    letter-spacing: 0;
	    text-align: center;
	    opacity: 0;
	    visibility: hidden;
	    color: #ff726b;
	}
	.calendarNormal {
	    width: 100%;
	    position: absolute;
	    bottom: 0;
	    font-size: 14px;
	    line-height: 30px;
	    font-weight: 600;
	    text-transform: uppercase;
	    letter-spacing: 3px;
	    text-align: center;
	    opacity: 0;
	    visibility: hidden;
	}
	.date2 {
	    color: #ff726b;
	}
	.day2 {
	    color: #3498DB;
	}
	.month2 {
	    color: #EFC94C;
	}
	/* -- usable codes end -- */

	/* -- unusable codes (text, logo, etc.) -- */

	.info {
	    width: 100%;
	    height: 25%;
	    position: absolute;
	    top: 15%;
	    text-align: center;
	    opacity: 0;
	}
	.info li {
	    width: 100%;
	}
	.hover,
	.click,
	.yeaa {
	    font-size: 14px;
	    line-height: 25px;
	    font-weight: 600;
	    text-transform: uppercase;
	    letter-spacing: 2px;
	    text-align: center;
	    bottom: 0;
	    opacity: 1;
	}
	.dribbble {
	    position: absolute;
	    top: -60px;
	    font-size: 14px;
	    opacity: 0;
	}
	em {
	    color: #ed4988;
	}
	.designer {
	    width: 100%;
	    height: 50%;
	    position: absolute;
	    bottom: 0;
	    text-align: center;
	    opacity: 0;
	}
	.designer li {
	    width: 100%;
	    position: absolute;
	    bottom: 30%;
	}
	.designer a {
	    width: 30px;
	    height: 30px;
	    display: block;
	    position: relative;
	    border-radius: 100%;
	    margin: auto;
	    color: rgba(46, 204, 113, 0.55);
	}
	.designer a:after {
	    position: absolute;
	    top: 0;
	    left: 40px;
	    font-size: 14px;
	    line-height: 33px;
	    font-weight: 600;
	    text-transform: uppercase;
	    letter-spacing: 2px;
	    white-space: nowrap;
	}
	.designer a:hover:after {
	    color: #2ecc71;
	}
	.designer img {
	    display: block;
	    border-radius: 100%;
	}
	body:hover .info,
	body:hover .designer {
	    opacity: 1;
	}
	::selection {
	    background: transparent;
	}
	::-moz-selection {
	    background: transparent;
	}
</style>
<!-- #page-title -->

<!-- /#page-title -->


<!-- #single-blog-post -->
<section id="single-blog-post">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<!-- .content-holder -->
				<div class="content-holder">
					<!-- .img-holder -->
					<div class="img-holder">
						<img src="<?php echo BASE_URL;?>template/img/tigath.jpg" alt="" style="width:50%;">
					</div>
					<!-- /.img-holder -->
					<!-- .post-title -->
					<div class="post-title">
						<h1>TI Gathering UMN 2017</h1>
					</div>
					<!-- /.post-title -->

					<!-- .content -->
					<div class="content">
						<p align="center" style="font-size:15pt;">TI Gathering returns as the closing event of FesTIval 2017. TI Gathering is where students and lecturers of UMN Computer Science come together to have some fun and know each other. The starry Awarding Night will also roll out the red carpet. Be the one of TI Gathering 2017 history makers!</p>
					</div>
					<!-- /.content -->
					
					<!-- .post-meta -->
					<!-- <div class="post-meta">
						Posted by <a href="#">Jhone</a> on <a href="#">dec 24,2014</a>
					</div> -->
					<!-- /.post-meta -->

					<div class="page-title" style="margin-top:15%;">
					    <!-- main codes start -->
					    <div class="signboard outer">
					        <div class="signboard front inner anim04c">
					            <li class="year anim04c">
					                <span></span>
					            </li>
					            <ul class="calendarMain anim04c">
					                <li class="month anim04c" style="margin-top:10%;">
					                    <span></span>
					                </li>
					                <li class="date anim04c" style="margin-top:15%;">
					                    <span></span>
					                </li>
					                <li class="day anim04c" style="margin-top:13%;">
					                    <span></span>
					                </li>
					            </ul>
					            <li class="clock minute anim04c">
					                <span></span>
					            </li>
					            <li class="calendarNormal date2 anim04c">
					                <span></span>
					            </li>
					        </div>
					        <div class="signboard left inner anim04c">
					            <li class="clock hour anim04c">
					                <span></span>
					            </li>
					            <li class="calendarNormal day2 anim04c">
					                <span></span>
					            </li>
					        </div>
					        <div class="signboard right inner anim04c">
					            <li class="clock second anim04c">
					                <span></span>
					            </li>
					            <li class="calendarNormal month2 anim04c">
					                <span></span>
					            </li>
					        </div>
					    </div>
					    <!-- main codes end -->

						<div style="margin-top:-3%;">
							<h1><i class="fa fa-clock-o"></i> 13.45 - end</h1>
							<h1><i class="fa fa-home"></i> Function Hall UMN</h1>
							<h1><i class="fa fa-female"></i> Pick a color of FesTIval logo!</h1>
						</div>

						<div class="content" style="margin-top:5%">
							<p align="center" style="font-size:15pt;">
								TI Gathering 2017 comes with, <strong><text style="color:red">racing games</text></strong>, <strong><text style="color:purple">dinner</text></strong>,
								<strong><text style="color:skyblue">performances</text></strong>, <strong><text style="color:green">awarding</text></strong>. Tickets are now on sale and it is worth for only <strong><text style="text-decoration: underline;text-decoration-color: red;">Rp 25.000,00</text></strong>. TI Gathering 2017 <strong>also invite you to show your <text style="color:pink;">talent</text> at <text style="color:orange">Awarding Night</text></strong>. Please contact <strong>Aldric (Line: devilionic)</strong> for both information.<br>See you at the biggest gathering party of UMN Computer Science!
							</p>
						</div>

					</div>

					<div class="content" style="margin-top:5%">
						
					</div>
					
				</div>
				<!-- /.content-holder -->

			</div>
		</div>
	</div>
</section>
<!-- /#single-blog-post -->

<!-- #event-sponsor -->
<!-- <section id="event-sponsor">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="section-title">
					<h1>EVENT SPONSOR</h1>
					<p>tar taro logo sponsor disini bagus kayanya...</p>
				</div>
			</div>
		</div>
		<div class="row sponsor-logo-row">
			<div class="col-lg-12">
				<ul class="sponsor-logo">
					<li>
						<div class="item"><img src="img/sponsor-logo/1.png" alt=""></div>
					</li>
					<li>
						<div class="item"><img src="img/sponsor-logo/2.png" alt=""></div>
					</li>
					<li>
						<div class="item"><img src="img/sponsor-logo/3.png" alt=""></div>
					</li>
					<li>
						<div class="item"><img src="img/sponsor-logo/4.png" alt=""></div>
					</li>
				</ul>
			</div>
		</div>
	</div> -->
</section>
<!-- /#event-sponsor -->

<?php include "view/include/footer.php" ?>
<?php include 'includes/script.php' ?>

</body>
<script type="text/javascript">
	$(document).ready(function () {

		var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ]; 
		var dayNames= [ "Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday" ];

		var newDate = new Date();
		newDate.setDate(newDate.getDate());


		setInterval( function() {
			var hours = new Date().getHours();
			$(".hour").html(( hours < 10 ? "0" : "" ) + hours);
		    var seconds = new Date().getSeconds();
			$(".second").html(( seconds < 10 ? "0" : "" ) + seconds);
		    var minutes = new Date().getMinutes();
			$(".minute").html(( minutes < 10 ? "0" : "" ) + minutes);
		    
		    $(".month span,.month2 span").text(monthNames[11]);
		    $(".date span,.date2 span").text("9");
		    $(".day span,.day2 span").text(dayNames[6]);
		}, 1000);	



		$(".outer").on({
		    mousedown:function(){
		        $(".dribbble").css("opacity","1");
		    },
		    mouseup:function(){
		        $(".dribbble").css("opacity","0");
		    }
		});



	});
</script>
</html>