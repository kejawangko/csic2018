<?php include 'view/include/header.php'; ?>
<style type="text/css">
	#subscribe-newsletter .gradient-overlay{
		padding: 55px 55px !important;
	}
	
	#subscribe-newsletter .gradient-overlay::before{
		background: #fcaf1a !important;
	}
</style>
<body>
	<!-- #banner -->
	<section id="about-us" class="gradient-overlay">
		<div class="container">
			<div class="row">
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<div class="col-lg-5 col-md-5 col-sm-5">
					<div class="app-box wow slideInLeft">
						<img style="width: 85% !important;margin-left: 10% !important;" src="<?php echo BASE_URL;?>template/img/resources/logo.png" alt="">
					</div>
				</div>

				<div class="col-lg-7 col-md-7 col-sm-7">
					<div class="section-title">
						<h1 style="color: black;">CSIC 2018</h1>
					</div>

					<p style="color: black;" align="justify">
						UMN Computer Science Innovative Challenge merupakan kompetisi program kreativitas dan inovasi mahasiswa pertama yang diselenggarakan oleh program studi Informatika UMN. Kegiatan ini merupakan inisiatif program studi Informatika UMN untuk mendorong mahasiswa Indonesia untuk berinovasi dan memberikan apresiasi kepada insan-insan muda penuh kreativitas yang telah berani untuk mempublikasikan ide-ide kreatif mereka kepada khalayak luas.
						<br><br>
						<br>
						<br>
						<br>
					</p>
					<ul>
						<li class="scrollToLink"><a class="dwnld-now hvr-underline-reveal" href="#upcoming-event" style="color: white;">LEARN MORE...</a></li>
					</ul>
					<br>
					<br>
					<br>
					<br>

				</div>
			</div>
		</div>
	</section>
	<!-- /#banner -->

	<!-- #upcoming-event -->
	<section id="upcoming-event">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title text-center">
						<h1>Our Events</h1>
						<p align="justify" style="text-align: center;font-weight: bold">We are waiting for your participation.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 text-center">
					<div class="content-holder">
					<!-- .img-holder -->
						<div class="img-holder" style="margin-bottom:50px !important;">
							<img src="<?php echo BASE_URL;?>template/img/csic.jpg" alt="" style="width:50%;">
						</div>
						<!-- /.img-holder -->
						<!-- .post-title -->
						<div class="post-title">
							<h1>Course-Net CSIC 2018</h1>
						</div>
						<!-- /.post-title -->

						<!-- .content -->
						<div class="content" style="text-align: center;">
							UMN Computer Science Innovative Challenge merupakan kompetisi program kreativitas dan inovasi mahasiswa pertama yang diselenggarakan oleh program studi Informatika UMN. Kegiatan ini merupakan inisiatif program studi Informatika UMN untuk mendorong mahasiswa Indonesia untuk berinovasi dan memberikan apresiasi kepada insan-insan muda penuh kreativitas yang telah berani untuk mempublikasikan ide-ide kreatif mereka kepada khalayak luas.<br><br>
							Rangkaian kegiatan CSIC terdiri dari sosialisasi dan publikasi CSIC, pendaftaran, pengumpulan purwarupa dan laporan, seleksi finalis, hingga akhirnya ditutup dengan presentasi dan penjurian akhir, serta penutupan dan pengumuman pemenang.<br><br>
							Adapun kategori yang dapat diikutsertakan dalam UMN Computer Science Innovative Challenge 2018 adalah sebagai berikut.
						</div>

						<div class="page-title" style="margin-top:5%;margin-bottom:30px !important">
							<h1>Our Category &#9759;</h1>
						</div>

						<div class="row">
						    <div class="col-md-12">
						    	<div class="row" style="margin-bottom: 20px !important;">
						    		<div class="col-lg-4"></div>
						    		<div class="col-lg-4">
						    			<h3>Usaha Kecil Menengah (UKM) Profit</h3>
						    		</div>
						    		<div class="col-lg-4"></div>
						    	</div>
						    	<div class="row" style="margin-bottom: 20px !important;">
						    		<div class="col-lg-4"></div>
						    		<div class="col-lg-4">
						    			<h3>Usaha Kecil Menegah (UKM) Non-profit</h3>
						    		</div>
						    		<div class="col-lg-4"></div>
						    	</div>
						    	<div class="row" style="margin-bottom: 20px !important;">
						    		<div class="col-lg-4"></div>
						    		<div class="col-lg-4">
						    			<h3>Kewirausahaan</h3>
						    		</div>
						    		<div class="col-lg-4"></div>
						    	</div>
						    	<!-- <div class="row" style="margin-bottom: 30px !important;">
							    	<div class="col-lg-2"></div>
							    	<div class="col-lg-4" style="border: 1px solid black; border-radius: 10px;">
							    		<h2>Enterpreneurship / Kewirausahaan</h2>
							    	</div>
							    	<div class="col-lg-4" style="border: 1px solid black; border-radius: 10px;">
							    		<h2>Community Service / Pengabdian kepada Masyarakat</h2>
							    	</div>
							    	<div class="col-lg-2"></div>
							    </div>
							    <div class="row">
							    	<div class="col-lg-2"></div>
							    	<div class="col-lg-4">
							    		<h2>Technology Application / Penerapan Teknologi</h2>
							    	</div>
							    	<div class="col-lg-4">
							    		<h2>Innovative Technology / Inovasi Teknologi</h2>
							    	</div>
							    	<div class="col-lg-2"></div>
							    </div> -->
						    </div>
						</div>

						<!-- <div class="page-title" style="margin-top:5%;">
							<h1>CSIC Finalist &#9759;</h1>
						</div> --> 
						<div class="row">
						    <div class="col-md-12">
						        <div>
						            <h1>Timeline</h1>
						        </div>
					            <ul class="timeline">
									<li style="margin-bottom: 30px;">
					                  <div class="posted-date">
					                    <span class="month" style="font-weight: normal !important">13 September 2018<br> sampai<br><span style="text-decoration: line-through red">14 Oktober 2018</span><br><span style="font-weight: bold">extended 'till 24 Oktober 2018</span></span>
					                  </div>
					                  <div class="timeline-panel">
					                    <div class="timeline-content">
					                      <div class="timeline-heading">
					                        <h3>Registrasi</h3>
					                      </div><!-- /timeline-heading -->
					                    </div> <!-- /timeline-content -->
					                  </div><!-- /timeline-panel -->
					                </li>
									<br><br>
									<li class="timeline-inverted">
					                  <div class="posted-date">
					                    <span class="month" style="font-weight: normal !important"><span style="text-decoration: line-through red">7 November 2018 </span><br><span style="font-weight: bold">extended 'till 12 November 2018</span></span>
					                  </div>
					                  <div class="timeline-panel">
					                    <div class="timeline-content">
					                      <div class="timeline-heading">
					                        <h3>Batas Pengumpulan Prototipe & Laporan</h3>
					                      </div><!-- /timeline-heading -->
					                    </div> <!-- /timeline-content -->
					                  </div><!-- /timeline-panel -->
					                </li>
					                <li>
					                  <div class="posted-date">
					                    <span class="month" style="font-weight: normal !important;">15 November 2018</span>
					                  </div>
					                  <div class="timeline-panel">
					                    <div class="timeline-content">
					                      <div class="timeline-heading">
					                        <h3>Pengumuman Finalis</h3>
					                      </div><!-- /timeline-heading -->
					                    </div> <!-- /timeline-content -->
					                  </div><!-- /timeline-panel -->
					                </li>
									<li class="timeline-inverted">
					                  <div class="posted-date">
					                    <span class="month" style="font-weight: normal !important">6 Desember 2018</span>
					                  </div>
					                  <div class="timeline-panel">
					                    <div class="timeline-content">
					                      <div class="timeline-heading">
					                        <h3>Presentasi dan Penjurian Akhir</h3>
					                      </div><!-- /timeline-heading -->
					                    </div> <!-- /timeline-content -->
					                  </div><!-- /timeline-panel -->
					                </li>
					            </ul>
						    </div>
						</div>

						<div style="margin-top:5%;">
							<ul>
								<!-- <li>
									<a href="<?php echo BASE_URL;?>csicFinalist" class="colored hvr-bounce-to-right" id="rulebook" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;margin-bottom: 1%">
										VIEW FINALIST
									</a>
								</li> -->
								<!-- <li>
									<a href="<?php echo BASE_URL;?>storage/Requirements-Final.pdf" class="colored hvr-bounce-to-right" id="rulebook" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;margin-bottom: 1%">
										FINAL PREPARATION
									</a>
								</li> -->
								<li>
									<a href="#" class="colored hvr-bounce-to-right" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;" id="btn-regis-selesai"> REGIST YOUR TEAM </a>
									<!-- <?php echo BASE_URL;?>csic/registration (ini link buat regis) -->
									<!-- <?php if(!isset($_SESSION['csic_team']['payment_proof'])){ ?>
									<a href="<?php echo BASE_URL;?>csic/registration" class="colored hvr-bounce-to-right" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;"> VIEW MY TEAM </a>
									<?php } else if(isset($_SESSION['csic_team']['payment_proof'])) {?>
									<a href="<?php echo BASE_URL;?>csic/registration" class="colored hvr-bounce-to-right" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;"> VIEW MY TEAM </a>
									<?php }?> -->
								</li>
								<li>
									<a href="<?php echo BASE_URL;?>storage/Rulebook CSIC 2018.pdf" class="colored hvr-bounce-to-right" id="rulebook" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;margin-top: 1%">
										RULEBOOK
									</a>
								</li>
								<li>
									<a href="<?php echo BASE_URL;?>storage/12 Finalists.pdf" class="colored hvr-bounce-to-right" id="rulebook" style="border:1px solid;border-radius: 5px;width:15%;height:35px;line-height: 32px;margin-top: 1%;font-weight: bold;">
										PENGUMUMAN FINALIS
									</a>
								</li>
							</ul>
						</div>

						<!-- /.content -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /#upcoming-event -->

	<!-- #gallery -->
	<section id="gallery">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title text-center">
						<h1>Our Gallery</h1>
						<p>Just a sneak peek of our last event.</p>
					</div>
				</div>
			</div>
			<div class="row gallery-image">
				<a class="fancybox" href="<?php echo BASE_URL;?>template/img/gallery/1-high.jpg"><div class="col-lg-3 col-md-4 col-sm-6">
					<img src="<?php echo BASE_URL;?>template/img/gallery/1.jpg" alt="">
				</div></a>
				<a class="fancybox" href="<?php echo BASE_URL;?>template/img/gallery/2-high.jpg"><div class="col-lg-3 col-md-4 col-sm-6">
					<img src="<?php echo BASE_URL;?>template/img/gallery/2.jpg" alt="">
				</div></a>
				<a class="fancybox" href="<?php echo BASE_URL;?>template/img/gallery/3-high.jpg"><div class="col-lg-3 col-md-4 col-sm-6">
					<img src="<?php echo BASE_URL;?>template/img/gallery/3.jpg" alt="">
				</div></a>
				<a class="fancybox" href="<?php echo BASE_URL;?>template/img/gallery/4-high.jpg"><div class="col-lg-3 col-md-4 col-sm-6">
					<img src="<?php echo BASE_URL;?>template/img/gallery/4.jpg" alt="">
				</div></a>
				<a class="fancybox" href="<?php echo BASE_URL;?>template/img/gallery/5-high.jpg"><div class="col-lg-3 col-md-4 col-sm-6">
					<img src="<?php echo BASE_URL;?>template/img/gallery/5.jpg" alt="">
				</div></a>
				<a class="fancybox" href="<?php echo BASE_URL;?>template/img/gallery/6-high.jpg"><div class="col-lg-3 col-md-4 col-sm-6">
					<img src="<?php echo BASE_URL;?>template/img/gallery/6.jpg" alt="">
				</div></a>
				<a class="fancybox" href="<?php echo BASE_URL;?>template/img/gallery/7-high.jpg"><div class="col-lg-3 col-md-4 col-sm-6">
					<img src="<?php echo BASE_URL;?>template/img/gallery/7.jpg" alt="">
				</div></a>
				<a class="fancybox" href="<?php echo BASE_URL;?>gallery"><div class="col-lg-3 col-md-4 col-sm-6">
					<img src="<?php echo BASE_URL;?>template/img/gallery/8.png" alt="">
				</div></a>
			</div>
		</div>
	</section>
	<!-- /#gallery -->

	<!-- #event-sponsor -->
	<section id="event-sponsor">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title">
						<h1>EVENT SPONSOR</h1>
						<!-- <p>Thank you for sponsor and media partner.</p> -->
						<!-- <hr> -->
					</div>
				</div>
			</div>
			<div class="row sponsor-logo-row">
				<div class="col-lg-12">
					<div class="item col-lg-4">
						<a href="http://www.course-net.com/" target="_blank" class="hvr-underline-reveal"><img src="<?php echo BASE_URL;?>template/img/sponsor-logo/coursenet.png" alt=""></a>
					</div>
				</div>
			</div>
			<hr>
			<br>
			<br>
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title">
						<h1>EVENT MEDIA PARTNER</h1>
						<!-- <p>Thank you for sponsor and media partner.</p> -->
						<!-- <hr> -->
					</div>
				</div>
			</div>
			<div class="row sponsor-logo-row">
				<div class="col-lg-12">
					<div class="item col-lg-2">
						<img src="<?php echo BASE_URL;?>template/img/medpar/1.jpg" alt="">
					</div>
					<div class="item col-lg-2" >
						<img src="<?php echo BASE_URL;?>template/img/medpar/2.png" alt="">
					</div>
					<div class="item col-lg-2">
						<img src="<?php echo BASE_URL;?>template/img/medpar/3.png" alt="">
					</div>
					<div class="item col-lg-2">
						<img src="<?php echo BASE_URL;?>template/img/medpar/4.jpg" alt="">
					</div>
					<div class="item col-lg-2" >
						<img src="<?php echo BASE_URL;?>template/img/medpar/5.jpg" alt="">
					</div>
					<div class="item col-lg-2" >
						<img src="<?php echo BASE_URL;?>template/img/medpar/6.jpg" alt="">
					</div>
				</div>
			</div>
			<div class="row sponsor-logo-row">
				<div class="col-lg-12">
					<div class="item col-lg-4">
						<img src="<?php echo BASE_URL;?>template/img/medpar/7.png" alt="">
					</div>
					<div class="item col-lg-4">
						<img src="<?php echo BASE_URL;?>template/img/medpar/8.png" alt="">
					</div>
				</div>
			</div>
			<h4>Supported by:</h4>
			<div class="row sponsor-logo-row">
				<div class="col-lg-12">
					<div class="item col-lg-6">
						<img src="<?php echo BASE_URL;?>template/img/medpar/mtp.png" alt="">
					</div>
				</div>
			</div>
			<hr>
		</div>
	</section>
	<!-- /#event-sponsor -->

	<!-- #subscribe-newsletter -->
	<section id="subscribe-newsletter">
		<div class="container">
			<div class="gradient-overlay" style="background: #fcaf1a !important;">
				<div class="row">
					<div class="col-lg-8 col-md-8 col-sm-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-1">
						<div class="text-box col-lg-6 col-md-6 col-sm-6" style="color: #1a0758 !important">
							Subscribe Our News
						</div>
						<div class="input-box col-lg-6 col-md-6 col-sm-6">
							<form action="<?php echo BASE_URL;?>subscribe" method="post">
								<input type="email" name="email" placeholder="Enter Email">
								<button type="submit" name="submit"><i class="fa fa-paper-plane"></i></button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /#subscribe-newsletter -->

	<?php include "view/include/footer.php" ?>

	<?php include "includes/script.php" ?>

	<script type="text/javascript">
		$(document).on('click','#btn-regis-selesai', function(){
			swal('','You can no longer regist for this competition! See you next competition','warning')
		})
	</script>

</body>
</html>