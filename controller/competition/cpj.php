<?php

    if(!isset($_POST["submit"])) {
        /*header("Location: ".BASE_URL."view/register.php");*/
    }
    else{
        $IName = filter_var($_POST['IName'],FILTER_SANITIZE_STRING);
        $name1 = filter_var($_POST['name1'],FILTER_SANITIZE_STRING);
        $email1 = filter_var($_POST['email1'],FILTER_SANITIZE_STRING);
        $phone1 = filter_var($_POST['phone1'],FILTER_SANITIZE_STRING);
		$size1 = filter_var($_POST['size1'],FILTER_SANITIZE_STRING);
        $name2 = filter_var($_POST['name2'],FILTER_SANITIZE_STRING);
        $email2 = filter_var($_POST['email2'],FILTER_SANITIZE_STRING);
        $phone2 = filter_var($_POST['phone2'],FILTER_SANITIZE_STRING);
		$size2 = filter_var($_POST['size2'],FILTER_SANITIZE_STRING);
       
        
        //upload skma
        $target_dir = "storage/competition/cpj/skma/";
        $target_file = $target_dir . $_SESSION['user_email'] . " - " . basename($_FILES["skma"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $check = getimagesize($_FILES["skma"]["tmp_name"]);
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimetype = finfo_file($finfo, $_FILES["skma"]["tmp_name"]);
        if($mimetype != "application/zip"){
            $_SESSION['skma_error'] = "Compressed all requirement such as SKMA or KTP in .zip format. Max file size: 10MB";
            $uploadOk = 0;
        }
        if ($_FILES["skma"]["size"] > 10000000) {
            $_SESSION['skma_error'] = "Compressed all requirement such as SKMA or KTP in .zip format. Max file size: 10MB";
            $uploadOk = 0;
        }
        if( $uploadOk == 0){
            header("Location: ".BASE_URL."cp/registerjcpc");
            exit();
        }
        if (move_uploaded_file($_FILES["skma"]["tmp_name"], $target_file)) {
            $pictureName = $target_dir. $_SESSION['user_email'] . " - " . basename( $_FILES["skma"]["name"]);

            $conn = new mysqli($servername, $username, $server_password, $dbname);
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            } 
            $user_id = $_SESSION['user_id'];
            $sql = "INSERT INTO teams(user_id, competition_id, title, institution, skma, payment_proof) VALUES ('$user_id', '4','-','$IName','$pictureName','-')";

            if($conn->query($sql) !== TRUE){
                $_SESSION['error'] = 'System occur an error #1, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
				$conn->close();
                header("Location: ".BASE_URL."cp/registerjcpc");
                exit();
            }

            $sql = "SELECT id FROM teams WHERE competition_id='4' AND user_id=$user_id";
            $result = $conn->query($sql);

            $row = $result->fetch_assoc();
            $team_id = $row["id"];

            $sql = "INSERT INTO members(team_id, full_name, email, phone, cloth_size) VALUES ('$team_id', '$name1', '$email1','$phone1','$size1')";

            if($conn->query($sql) !== TRUE){
                $_SESSION['error'] = 'System occur an error #2, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
                $conn->close();
                header("Location: ".BASE_URL."cp/registerjcpc");
                exit();
            }

            $sql = "INSERT INTO members(team_id, full_name, email, phone, cloth_size) VALUES ('$team_id', '$name2', '$email2','$phone2','$size2')";

            if($conn->query($sql) !== TRUE){
                $_SESSION['error'] = 'System occur an error #3, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
                $conn->close();
                header("Location: ".BASE_URL."cp/registerjcpc");
                exit();
            }

           
            $conn->close();
        }
        else {
            $_SESSION['error'] = 'System occur an error, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
            header("Location: ".BASE_URL."cp/registerjcpc");
            exit();
        }
        header("Location: ".BASE_URL."cp/registerjcpc");
        exit();
    }
?>