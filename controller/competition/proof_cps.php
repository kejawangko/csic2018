<?php

    if(!isset($_POST["submit"])) {
        /*header("Location: ".BASE_URL."view/register.php");*/
    }
    else{
        //upload proof
        date_default_timezone_set("Asia/Jakarta");

        $target_dir = "storage/competition/cps/proof/";
        $target_file = $target_dir . $_SESSION['user_email'] . " - " . basename($_FILES["payment_proof"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $check = getimagesize($_FILES["payment_proof"]["tmp_name"]);
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimetype = finfo_file($finfo, $_FILES["payment_proof"]["tmp_name"]);
        if($mimetype != "application/pdf" && $mimetype != "image/jpeg"){
            $_SESSION['payment_proof_error'] = "Please input Payment Proof in PDF / JPEG format. Max file size: 10MB";
            $uploadOk = 0;
        }
        if ($_FILES["payment_proof"]["size"] > 10000000) {
            $_SESSION['payment_proof_error'] = "Please input Payment Proof in PDF / JPEG format. Max file size: 10MB";
            $uploadOk = 0;
        }
        if( $uploadOk == 0){
            header("Location: ".BASE_URL."cp/registerscpc");
            exit();
        }
        if (move_uploaded_file($_FILES["payment_proof"]["tmp_name"], $target_file)) {
            $pictureName = $target_dir. $_SESSION['user_email'] . " - " . basename( $_FILES["payment_proof"]["name"]);

            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            } 

            $team_id = $_SESSION['cps_team']['id'];
            $date_time = date("Y-m-d H:i:s");

            $sql = "UPDATE teams SET payment_proof='$pictureName', created_at='$date_time' WHERE id='$team_id'";

            if($conn->query($sql) !== TRUE){
                $_SESSION['error'] = 'System occur an error #1, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
                $conn->close();
                header("Location: ".BASE_URL."cp/registerscpc");
                exit();
            }
            $conn->close();
        }
        else {
            $_SESSION['error'] = 'System occur an error, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
            header("Location: ".BASE_URL."cp/registerscpc");
            exit();
        }
        echo BASE_URL;
        header("Location: ".BASE_URL."cp/registerscpc");
        exit();
    }
?>