<?php

    if(!isset($_POST["submit"])) {
        /*header("Location: ".BASE_URL."view/register.php");*/
    }
    else{

        $IName = filter_var($_POST['IName'],FILTER_SANITIZE_STRING);
        $name1 = filter_var($_POST['name1'],FILTER_SANITIZE_STRING);
        $email1 = filter_var($_POST['email1'],FILTER_SANITIZE_STRING);
        $phone1 = filter_var($_POST['phone1'],FILTER_SANITIZE_STRING);
		$size1 = filter_var($_POST['size1'],FILTER_SANITIZE_STRING);
        $name2 = filter_var($_POST['name2'],FILTER_SANITIZE_STRING);
        $email2 = filter_var($_POST['email2'],FILTER_SANITIZE_STRING);
        $phone2 = filter_var($_POST['phone2'],FILTER_SANITIZE_STRING);
		$size2 = filter_var($_POST['size2'],FILTER_SANITIZE_STRING);
        
        //upload skma
        $target_dir = "storage/competition/cpj/skma/";
        $target_file = $target_dir . $_SESSION['user_email'] . " - " . basename($_FILES["skma"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $check = getimagesize($_FILES["skma"]["tmp_name"]);
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimetype = finfo_file($finfo, $_FILES["skma"]["tmp_name"]);
        if($mimetype != "application/zip"){
            $_SESSION['skma_error'] = "Compressed all requirement such as SKMA or KTP in .zip format. Max file size: 10MB";
            $uploadOk = 0;
        }
        if ($_FILES["skma"]["size"] > 10000000) {
            $_SESSION['skma_error'] = "Compressed all requirement such as SKMA or KTP in .zip format. Max file size: 10MB";
            $uploadOk = 0;
        }
        if( $uploadOk == 0){
            header("Location: ".BASE_URL."cp/registerjcpc");
            exit();
        }
        if (move_uploaded_file($_FILES["skma"]["tmp_name"], $target_file)) {
            $pictureName = $target_dir. $_SESSION['user_email'] . " - " . basename( $_FILES["skma"]["name"]);

            $conn = new mysqli($servername, $username, $server_password, $dbname);
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            } 
			var_dump($_SESSION['cpj_team']);
            $team_id = $_SESSION['cpj_team']['id'];
            $sql = "UPDATE teams SET institution='$IName', skma='$pictureName' WHERE id='$team_id'";

            if($conn->query($sql) !== TRUE){
                $_SESSION['error'] = 'System occur an error #1, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
                $conn->close();
                header("Location: ".BASE_URL."cp/registerjcpc");
            }

            $member_id1 = $_SESSION['cpj_member'][0]['id'];
            $sql = "UPDATE members SET full_name='$name1', email='$email1', cloth_size='$size1' , phone='$phone1' WHERE team_id='$team_id' AND id='$member_id1'";

            if($conn->query($sql) !== TRUE){
                $_SESSION['error'] = 'System occur an error #2, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
                $conn->close();
                header("Location: ".BASE_URL."cp/registerjcpc");
            }

            $member_id2 = $_SESSION['cpj_member'][1]['id'];
            $sql = "UPDATE members SET full_name='$name2', email='$email2',cloth_size='$size2', phone='$phone2' WHERE team_id='$team_id'  AND id='$member_id2'";
            var_dump($sql);
            if($conn->query($sql) !== TRUE){
                $_SESSION['error'] = 'System occur an error #3, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
                $conn->close();
                header("Location: ".BASE_URL."cp/registerjcpc");
            }

            $conn->close();
        }
        else {
            $_SESSION['error'] = 'System occur an error, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
            header("Location: ".BASE_URL."cp/registerjcpc");
        }
        header("Location: ".BASE_URL."cp/registerjcpc");
    }
?>