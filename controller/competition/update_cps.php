<?php

    if(!isset($_POST["submit"])) {
        /*header("Location: ".BASE_URL."view/register.php");*/
    }
    else{

        $IName = filter_var($_POST['IName'],FILTER_SANITIZE_STRING);
        $TeamName = filter_var($_POST['TeamName'],FILTER_SANITIZE_STRING);
        $name1 = filter_var($_POST['name1'],FILTER_SANITIZE_STRING);
        $email1 = filter_var($_POST['email1'],FILTER_SANITIZE_STRING);
        $phone1 = filter_var($_POST['phone1'],FILTER_SANITIZE_STRING);
		$size1 = filter_var($_POST['size1'],FILTER_SANITIZE_STRING);
        $name2 = filter_var($_POST['name2'],FILTER_SANITIZE_STRING);
        $email2 = filter_var($_POST['email2'],FILTER_SANITIZE_STRING);
        $phone2 = filter_var($_POST['phone2'],FILTER_SANITIZE_STRING);
		$size2 = filter_var($_POST['size2'],FILTER_SANITIZE_STRING);
        $name3 = filter_var($_POST['name3'],FILTER_SANITIZE_STRING);
        $email3 = filter_var($_POST['email3'],FILTER_SANITIZE_STRING);
        $phone3 = filter_var($_POST['phone3'],FILTER_SANITIZE_STRING);
		$size3 = filter_var($_POST['size3'],FILTER_SANITIZE_STRING);
        $name4 = filter_var($_POST['name4'],FILTER_SANITIZE_STRING);
        $email4 = filter_var($_POST['email4'],FILTER_SANITIZE_STRING);
        $phone4 = filter_var($_POST['phone4'],FILTER_SANITIZE_STRING);
		$size4 = filter_var($_POST['size4'],FILTER_SANITIZE_STRING);
        
        //upload skma
        $target_dir = "storage/competition/cps/skma/";
        $target_file = $target_dir . $_SESSION['user_email'] . " - " . basename($_FILES["skma"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $check = getimagesize($_FILES["skma"]["tmp_name"]);
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimetype = finfo_file($finfo, $_FILES["skma"]["tmp_name"]);
        if($mimetype != "application/zip"){
            $_SESSION['skma_error'] = "Compressed all requirement such as SKMA or KTP in .zip format. Max file size: 10MB";
            $uploadOk = 0;
        }
        if ($_FILES["skma"]["size"] > 10000000) {
            $_SESSION['skma_error'] = "Compressed all requirement such as SKMA or KTP in .zip format. Max file size: 10MB";
            $uploadOk = 0;
        }
        if( $uploadOk == 0){
            header("Location: ".BASE_URL."cp/registerscpc");
            exit();
        }
        if (move_uploaded_file($_FILES["skma"]["tmp_name"], $target_file)) {
            $pictureName = $target_dir. $_SESSION['user_email'] . " - " . basename( $_FILES["skma"]["name"]);

            $conn = new mysqli($servername, $username, $server_password, $dbname);
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            } 
            $team_id = $_SESSION['cps_team']['id'];
            $sql = "UPDATE teams SET institution='$IName', title='$TeamName', skma='$pictureName' WHERE id='$team_id'";

            if($conn->query($sql) !== TRUE){
                $_SESSION['error'] = 'System occur an error #1, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
                $conn->close();
                header("Location: ".BASE_URL."cp/registerscpc");
            }

            $member_id1 = $_SESSION['cps_member'][0]['id'];
            $sql = "UPDATE members SET full_name='$name1', email='$email1', phone='$phone1', cloth_size='$size1' WHERE team_id='$team_id' AND id='$member_id1'";

            if($conn->query($sql) !== TRUE){
                $_SESSION['error'] = 'System occur an error #2, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
                $conn->close();
                header("Location: ".BASE_URL."cp/registerscpc");
            }

            $member_id2 = $_SESSION['cps_member'][1]['id'];
            $sql = "UPDATE members SET full_name='$name2', email='$email2', phone='$phone2', cloth_size='$size2' WHERE team_id='$team_id'  AND id='$member_id2'";

            if($conn->query($sql) !== TRUE){
                $_SESSION['error'] = 'System occur an error #3, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
                $conn->close();
                header("Location: ".BASE_URL."cp/registerscpc");
            }

            $member_id3 = $_SESSION['cps_member'][2]['id'];
            $sql = "UPDATE members SET full_name='$name3', email='$email3', phone='$phone3', cloth_size='$size3' WHERE team_id='$team_id'  AND id='$member_id3'";

            if($conn->query($sql) !== TRUE){
                $_SESSION['error'] = 'System occur an error #4, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
                $conn->close();
                header("Location: ".BASE_URL."cp/registerscpc");
            }

            $member_id4 = $_SESSION['cps_member'][3]['id'];
            $sql = "UPDATE members SET full_name='$name4', email='$email4', phone='$phone4', cloth_size='$size4' WHERE team_id='$team_id' AND id='$member_id4'";

            if($conn->query($sql) !== TRUE){
                $_SESSION['error'] = 'System occur an error #5, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
                $conn->close();
                header("Location: ".BASE_URL."cp/registerscpc");
            }

            $conn->close();
        }
        else {
            $_SESSION['error'] = 'System occur an error, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
            header("Location: ".BASE_URL."cp/registerscpc");
        }
        header("Location: ".BASE_URL."cp/registerscpc");
    }
?>