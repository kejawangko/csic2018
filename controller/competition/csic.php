<?php

    if(!isset($_POST["submit"])) {
        /*header("Location: ".BASE_URL."view/register.php");*/
    }
    else{

        $TeamName = filter_var($_POST['TeamName'],FILTER_SANITIZE_STRING);
        $IName = filter_var($_POST['IName'],FILTER_SANITIZE_STRING);
        $name1 = filter_var($_POST['name1'],FILTER_SANITIZE_STRING);
        $email1 = filter_var($_POST['email1'],FILTER_SANITIZE_STRING);
        $phone1 = filter_var($_POST['phone1'],FILTER_SANITIZE_STRING);
        $name2 = filter_var($_POST['name2'],FILTER_SANITIZE_STRING);
        $email2 = filter_var($_POST['email2'],FILTER_SANITIZE_STRING);
        $phone2 = filter_var($_POST['phone2'],FILTER_SANITIZE_STRING);
        $name3 = filter_var($_POST['name3'],FILTER_SANITIZE_STRING);
        $email3 = filter_var($_POST['email3'],FILTER_SANITIZE_STRING);
        $phone3 = filter_var($_POST['phone3'],FILTER_SANITIZE_STRING);
        $name4 = filter_var($_POST['name4'],FILTER_SANITIZE_STRING); 
        $email4 = filter_var($_POST['email4'],FILTER_SANITIZE_STRING);
        $phone4 = filter_var($_POST['phone4'],FILTER_SANITIZE_STRING);

        $validation = 1;

        if($TeamName == ''){
            $_SESSION['TeamName_error'] = 'Please fill in your Team Name.';
            $validation = 0;
        }
        if($IName == ''){
            $_SESSION['IName_error'] = 'Please fill in your Institution Name.';
            $validation = 0;
        }
        if($name1 == ''){
            $_SESSION['name1_error'] = 'Please fill in your first team nember name.';
            $validation = 0;
        }
        if($email1 == ''){
            $_SESSION['email1_error'] = 'Please fill in your first team nember email.';
            $validation = 0;
        }
        if($phone1 == ''){
            $_SESSION['phone1_error'] = 'Please fill in your first team nember phone number.';
            $validation = 0;
        }
        if($name2 == ''){
            $_SESSION['name2_error'] = 'Please fill in your first team nember name.';
            $validation = 0;
        }
        if($email2 == ''){
            $_SESSION['email2_error'] = 'Please fill in your first team nember email.';
            $validation = 0;
        }
        if($phone2 == ''){
            $_SESSION['phone2_error'] = 'Please fill in your first team nember phone number.';
            $validation = 0;
        }
        
        if($validation == 0 ){
            header("Location: ".BASE_URL."csic/registration");
            exit();
        }
        //upload skma
        $target_dir = "storage/competition/csic/skma/";
        $target_file = $target_dir . $_SESSION['user_email'] . " - " . basename($_FILES["skma"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $check = getimagesize($_FILES["skma"]["tmp_name"]);
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimetype = finfo_file($finfo, $_FILES["skma"]["tmp_name"]);
        if($mimetype != "application/zip"){
            $_SESSION['skma_error'] = "Compressed all requirement such as Proof of Eligibility, Student card, etc in .zip format. Max file size: 10MB";
            $uploadOk = 0;
        }
        if ($_FILES["skma"]["size"] > 10000000) {
            $uploadOk = 0;
            $_SESSION['skma_error'] = "Compressed all requirement such as Proof of Eligibility, Student card, etc in .zip format. Max file size: 10MB";
        }
        if( $uploadOk == 0){
            header("Location: ".BASE_URL."csic/registration");
            exit();
        }
        if (move_uploaded_file($_FILES["skma"]["tmp_name"], $target_file)) {
            $pictureName = $target_dir. $_SESSION['user_email'] . " - " . basename( $_FILES["skma"]["name"]);

            $conn = new mysqli($servername, $username, $server_password, $dbname);
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            } 
            $user_id = $_SESSION['user_id'];
            $sql = "INSERT INTO teams(user_id, competition_id, title, institution, skma, payment_proof) VALUES ('$user_id', '2','$TeamName','$IName','$pictureName','-')";

            if($conn->query($sql) !== TRUE){
                $_SESSION['error'] = 'System occur an error #1, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
                $conn->close();
                header("Location: ".BASE_URL."csic/registration");
                exit();
            }

            $sql = "SELECT id FROM teams WHERE competition_id='2' AND user_id=$user_id";
            $result = $conn->query($sql);

            $row = $result->fetch_assoc();
            $team_id = $row["id"];

            $sql = "INSERT INTO members(team_id, full_name, email, phone, cloth_size) VALUES ('$team_id', '$name1', '$email1','$phone1','-')";

            if($conn->query($sql) !== TRUE){
                $_SESSION['error'] = 'System occur an error #2, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
                $conn->close();
                header("Location: ".BASE_URL."csic/registration");
                exit();
            }

            $sql = "INSERT INTO members(team_id, full_name, email, phone, cloth_size) VALUES ('$team_id', '$name2', '$email2','$phone2','-')";

            if($conn->query($sql) !== TRUE){
                $_SESSION['error'] = 'System occur an error #3, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
                $conn->close();
                header("Location: ".BASE_URL."csic/registration");
                exit();
            }

            if($name3 != ''){
                $sql = "INSERT INTO members(team_id, full_name, email, phone, cloth_size) VALUES ('$team_id', '$name3', '$email3','$phone3','-')";

                if($conn->query($sql) !== TRUE){
                    $_SESSION['error'] = 'System occur an error #4, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
                    $conn->close();
                    header("Location: ".BASE_URL."csic/registration");
                    exit();
                }
            }
            else{
                $sql = "INSERT INTO members(team_id, full_name, email, phone) VALUES ('$team_id', '$name3', '$email3','$phone3')";

                if($conn->query($sql) !== TRUE){
                    $_SESSION['error'] = 'System occur an error #4, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
                    $conn->close();
                    header("Location: ".BASE_URL."csic/registration");
                    exit();
                }
            }

            if($name4 != ''){
                $sql = "INSERT INTO members(team_id, full_name, email, phone, cloth_size) VALUES ('$team_id', '$name4', '$email4','$phone4','-')";
                if($conn->query($sql) !== TRUE){
                    $_SESSION['error'] = 'System occur an error #5, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
                    $conn->close();
                    header("Location: ".BASE_URL."csic/registration");
                    exit();
                }
            }
            else{
                $sql = "INSERT INTO members(team_id, full_name, email, phone) VALUES ('$team_id', '$name4', '$email4','$phone4')";
                if($conn->query($sql) !== TRUE){
                    $_SESSION['error'] = 'System occur an error #5, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
                    $conn->close();
                    header("Location: ".BASE_URL."csic/registration");
                    exit();
                }
            }

            $conn->close();
        }
        else {
            $_SESSION['error'] = 'System occur an error, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
            header("Location: ".BASE_URL."csic/registration");
            exit();
        }
        $_SESSION['register_team_success'] = 'Congratulation! You have just create a new team.';
        header("Location: ".BASE_URL."csic/registration");
        exit();
    }
?>