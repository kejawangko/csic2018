<?php
    if(!isset($_POST["submit"])) {
        /*header("Location: ".BASE_URL."view/register.php");*/
    }
    else{

        date_default_timezone_set("Asia/Jakarta");

        $proposal = filter_var($_POST['proposal'],FILTER_SANITIZE_STRING);

        $validation = 1;

        if($proposal == ''){
            $_SESSION['proposal_error'] = 'Please fill in your Proposal link.';
            $validation = 0;
        }

        if($validation == 0 ){
            header("Location: ".BASE_URL."csic/registration");
        }

        $team_id = $_SESSION['csic_team']['id'];
        $date_time = date("Y-m-d H:i:s");
        $sql = "UPDATE teams SET proposal='$proposal', updated_at='$date_time' WHERE id='$team_id'";

        if($conn->query($sql) !== TRUE){
            $_SESSION['error'] = 'System occur an error #1, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
            $conn->close();
            header("Location: ".BASE_URL."csic/registration");
        }
        $conn->close();
        $_SESSION['csic_proposal_success'] = 'Success';
        header("Location: ".BASE_URL."csic/registration");
    }
?>