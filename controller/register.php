<?php

    if(!isset($_POST["submit"])) {
        /*header("Location: ".BASE_URL."view/register.php");*/
    }
    else{
        $name = filter_var($_POST['name'],FILTER_SANITIZE_STRING);
        $email = filter_var($_POST['email'],FILTER_SANITIZE_STRING);
        $password = filter_var($_POST['password'],FILTER_SANITIZE_STRING);
        $password_confirmation = filter_var($_POST['password_confirmation'],FILTER_SANITIZE_STRING);

        $validation = 1;

        if($name == ''){
            $_SESSION['error'] = 'Please fill in your Name.';
            $validation = 0;
        }
        if($email == ''){
            $_SESSION['error'] = 'Please fill in your Email.';
            $validation = 0;
        }
        if($password == ''){
            $_SESSION['error'] = 'Please fill in your Password.';
            $validation = 0;
        }
        if($password_confirmation == ''){
            $_SESSION['error'] = 'Please fill in your Password Confirmation.';
            $validation = 0;
        }
        
        if($validation == 0 ){
            header("Location: ".BASE_URL."register");
            exit();
        }

        $sql = "SELECT email FROM users WHERE email='$email'";
        $result = $conn->query($sql);
        if($result->num_rows > 0) {
            $_SESSION['register_success'] = 'Email is taken.';
            $conn->close();
            header("Location: ".BASE_URL."register");
            exit();
        }
        if($password != $password_confirmation){
            $_SESSION['password_unmatch'] = 'Password and password confrimation doesn\'t match.';
            $conn->close();
            header("Location: ".BASE_URL."register");
            exit();
        }
        
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        
        $password = md5($name.$password);

        $sql = "INSERT INTO users(fullname, email, password) VALUES ('$name', '$email','$password')";
        if($conn->query($sql) !== TRUE){
            $_SESSION['database_error'] = 'System occur an error, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
        }
        else {
            $_SESSION['register_success'] = 'Registration Success, please login.';
            echo "<script>";
            echo "$(document).ready(function () {
                        swal({
                            title:'Registration Success',
                            text:'You can use your account now!',
                            type:'success'
                            html: true
                        });
                    });";
            echo "</script>";
            header("Location: ".BASE_URL."login");
        }
        /*if ($conn->query($sql) !== TRUE) {
            $errorCode = 7;
        }*/
        $conn->close();
        //header("Location: ".BASE_URL."login");
    }
?>