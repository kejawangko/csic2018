<?php
	if(!isset($_POST["submit"])) {
        /*header("Location: ".BASE_URL."view/register.php");*/
    }
    else{
    	$oldPassword = filter_var($_POST['oldPassword'],FILTER_SANITIZE_STRING);
        $newPassword = filter_var($_POST['newPassword'],FILTER_SANITIZE_STRING);
        $newPasswordConfirmation = filter_var($_POST['newPasswordConfirmation'],FILTER_SANITIZE_STRING);

        $email = $_SESSION['user_email'];

        $sql = "SELECT password FROM users WHERE email='$email'";
        $result = $conn->query($sql);

        $row = $result->fetch_assoc();
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 

        $realPassword = $row["password"];
        $full_name = $_SESSION['user_fullname'];
        $oldPassword = md5($full_name.$oldPassword);
        if($realPassword != $oldPassword){
        	$conn->close();
        	$_SESSION['change_password'] = 'Change password failed, your old password doesn\'t match our record.';
        	header("Location: ".BASE_URL."changePassword");
        	exit();
        }
        else if($newPassword != $newPasswordConfirmation){
        	$conn->close();
        	$_SESSION['change_password'] = 'Your new password unmatch.';
        	header("Location: ".BASE_URL."changePassword");
        	exit();
        }
        $newPassword = md5($full_name.$newPassword);

        $sql = "UPDATE users SET password='$newPassword' WHERE email='$email'";
        $result = $conn->query($sql);
        
        if($conn->query($sql) !== TRUE){
            $_SESSION['change_password'] = 'System occur an error #1, please try again in a few minutes or feel free to contact himti.festival@umn.ac.id';
            $conn->close();
            header("Location: ".BASE_URL."changePassword");
            exit();
        }
        $conn->close();
        $_SESSION['change_password'] = 'Password has been changed.';
    	header("Location: ".BASE_URL."changePassword");
    	exit();
    }

?>