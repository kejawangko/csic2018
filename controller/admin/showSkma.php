<?php

	require_once( 'includes/url.php');

	

	$sql = "SELECT t.id 'id', t.title 'title', t.institution 'institution', count(m.id) 'total', t.skma 'skma', t.payment_proof 'payment_proof', t.proposal, t.created_at, t.updated_at 

			FROM teams t JOIN members m ON (t.id = m.team_id) 

			WHERE competition_id='1' 

			GROUP BY t.id";

	$result = $conn->query($sql);

	$team = array();

	if($result != false) {

		$counter = 0;

		while($row = $result->fetch_assoc()) {

			$team[$counter]['id'] = $row["id"];

			$team[$counter]['title'] = $row["title"];

			$team[$counter]['institution'] = $row["institution"];

			$team[$counter]['skma'] = $row["skma"];

			$team[$counter]['total'] = $row["total"];

			$team[$counter]['payment_proof'] = $row["payment_proof"];

			$team[$counter]['proposal'] = $row["proposal"];

			$team[$counter]['payment_time'] = $row["created_at"];

			$team[$counter]['proposal_time'] = $row["updated_at"];

			$counter++;

		}

	}
	
	$_SESSION['hackathon_skma'] = $team;



	$sql = "SELECT t.id 'id', t.title 'title', t.institution 'institution', count(m.cloth_size) 'total', t.skma 'skma', t.payment_proof 'payment_proof', t.proposal, t.created_at, t.updated_at, GROUP_CONCAT(m.full_name SEPARATOR '<br>') as member_names 

			FROM teams t JOIN members m ON (t.id = m.team_id)

			WHERE t.competition_id='2' AND t.created_at > '2018-01-01'

			GROUP BY t.id";

	$result = $conn->query($sql);

	$team = array();

	if($result->num_rows > 0) {

		$counter = 0;

		while($row = $result->fetch_assoc()) {

			$team[$counter]['id'] = $row["id"];

			$team[$counter]['title'] = $row["title"];

			$team[$counter]['institution'] = $row["institution"];

			$team[$counter]['skma'] = $row["skma"];

			$team[$counter]['member_names'] = $row["member_names"];

			$team[$counter]['total'] = $row["total"];

			$team[$counter]['payment_proof'] = $row["payment_proof"];

			$team[$counter]['proposal'] = $row["proposal"];

			$team[$counter]['payment_time'] = $row["created_at"];

			$team[$counter]['proposal_time'] = $row["updated_at"];

			$counter++;

		}

	}

	$_SESSION['csic_skma'] = $team;



	$sql = "SELECT t.id 'id', t.title 'title', t.institution 'institution', count(m.id) 'total', t.skma 'skma', t.payment_proof 'payment_proof', t.proposal, t.created_at, t.updated_at, GROUP_CONCAT(m.full_name SEPARATOR '<br>') as member_names 

			FROM teams t JOIN members m ON (t.id = m.team_id)

			WHERE competition_id='3' AND LENGTH(m.full_name)>0

			GROUP BY t.id";

	$result = $conn->query($sql);

	$team = array();

	if($result->num_rows > 0) {

		$counter = 0;

		while($row = $result->fetch_assoc()) {

			$team[$counter]['id'] = $row["id"];

			$team[$counter]['title'] = $row["title"];

			$team[$counter]['institution'] = $row["institution"];

			$team[$counter]['skma'] = $row["skma"];

			$team[$counter]['total'] = $row["total"];

			$team[$counter]['member_names'] = $row["member_names"];

			$team[$counter]['payment_proof'] = $row["payment_proof"];

			$team[$counter]['proposal'] = $row["proposal"];

			$team[$counter]['payment_time'] = $row["created_at"];

			$team[$counter]['proposal_time'] = $row["updated_at"];

			$counter++;

		}

	}

	$_SESSION['scp_skma'] = $team;



	$sql = "SELECT t.id 'id', t.title 'title', t.institution 'institution', count(m.id) 'total', t.skma 'skma', t.payment_proof 'payment_proof', t.proposal, t.created_at, t.updated_at, GROUP_CONCAT(m.full_name SEPARATOR '<br>') as member_names 

			FROM teams t JOIN members m ON (t.id = m.team_id)

			WHERE competition_id='4' AND LENGTH(m.full_name)>0

			GROUP BY t.id";

	$result = $conn->query($sql);



	$team = array();

	if($result->num_rows > 0) {

		$counter = 0;

		while($row = $result->fetch_assoc()) {

			$team[$counter]['id'] = $row["id"];

			$team[$counter]['title'] = $row["title"];

			$team[$counter]['institution'] = $row["institution"];

			$team[$counter]['skma'] = $row["skma"];

			$team[$counter]['total'] = $row["total"];

			$team[$counter]['member_names'] = $row["member_names"];

			$team[$counter]['payment_proof'] = $row["payment_proof"];

			$team[$counter]['proposal'] = $row["proposal"];

			$team[$counter]['payment_time'] = $row["created_at"];

			$team[$counter]['proposal_time'] = $row["updated_at"];

			$counter++;

		}

	}

	$_SESSION['jcp_skma'] = $team;



	

?>