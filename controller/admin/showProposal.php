<?php
	require_once( 'includes/url.php');
	
	$sql = "SELECT id, title, institution, proposal FROM teams WHERE competition_id='1'";
	$result = $conn->query($sql);
	
	$team = array();
	$counter = 0;
	while($row = $result->fetch_assoc()) {
		$team[$counter]['id'] = $row["id"];
		$team[$counter]['title'] = $row["title"];
		$team[$counter]['institution'] = $row["institution"];
		$team[$counter]['proposal'] = $row["proposal"];
		$counter++;
	}
	$_SESSION['hackathon_proposal'] = $team;

	$sql = "SELECT id, title, institution, proposal FROM teams WHERE competition_id='2'";
	$result = $conn->query($sql);
	
	$team = array();
	$counter = 0;
	while($row = $result->fetch_assoc()) {
		$team[$counter]['id'] = $row["id"];
		$team[$counter]['title'] = $row["title"];
		$team[$counter]['institution'] = $row["institution"];
		$team[$counter]['proposal'] = $row["proposal"];
		$counter++;
	}
	$_SESSION['csic_proposal'] = $team;

	$sql = "SELECT id, title, institution, proposal FROM teams WHERE competition_id='3'";
	$result = $conn->query($sql);
	
	$team = array();
	$counter = 0;
	while($row = $result->fetch_assoc()) {
		$team[$counter]['id'] = $row["id"];
		$team[$counter]['title'] = $row["title"];
		$team[$counter]['institution'] = $row["institution"];
		$team[$counter]['proposal'] = $row["proposal"];
		$counter++;
	}
	$_SESSION['scp_proposal'] = $team;

	$sql = "SELECT id, title, institution, proposal FROM teams WHERE competition_id='4'";
	$result = $conn->query($sql);
	
	$team = array();
	$counter = 0;
	while($row = $result->fetch_assoc()) {
		$team[$counter]['id'] = $row["id"];
		$team[$counter]['title'] = $row["title"];
		$team[$counter]['institution'] = $row["institution"];
		$team[$counter]['proposal'] = $row["proposal"];
		$counter++;
	}
	$_SESSION['jcp_proposal'] = $team;

	
?>