<?php
	if(!isset($_POST["submit"])) {
        /*header("Location: ".BASE_URL."view/register.php");*/
    }
    else{
       $email = filter_var($_POST['email'],FILTER_SANITIZE_STRING);
        
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        $sql = "SELECT fullname FROM users WHERE email='$email'";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
	        $row = $result->fetch_assoc();
	        $fullname = $row["fullname"];
	        $realPassword = rand(100000,999999);
	        $password = md5($fullname.$realPassword);

	        $sql = "UPDATE users SET password='$password' WHERE email='$email'";
	        $result = $conn->query($sql);
	        $conn->close();

	        //sending email
	        $encoding = "utf-8";

		    // Preferences for Subject field
		    $subject_preferences = array(
		        "input-charset" => $encoding,
		        "output-charset" => $encoding,
		        "line-length" => 70,
		        "line-break-chars" => "\r\n"
		    );

	        // The message
			$message = "Hi $fullname,\r\n
						\r\n
						You have request a forget password email from CSIC with this email.\r\n
						Here is your new password: $realPassword";
			$mail_subject = 'CSIC Forget Password';
			$from_name = 'CSIC';
			$from_mail = 'csic.umn@gmail.com';

			$header = "Content-type: text/html; charset=".$encoding." \r\n";
		    $header .= "From: ".$from_name." <".$from_mail."> \r\n";
		    $header .= "MIME-Version: 1.0 \r\n";
		    $header .= "Content-Transfer-Encoding: 8bit \r\n";
		    $header .= "Date: ".date("r (T)")." \r\n";
		    $header .= iconv_mime_encode("Subject", $mail_subject, $subject_preferences);

			// Send
			/*ini_set("SMTP","ssl://smtp.gmail.com");
			ini_set("smtp_port","465");*/
			/*ini_set('SMTP','himti.umn.ac.id');
			ini_set('smtp_port',25);*/
			mail($email, $mail_subject, $message, $header);
			$_SESSION['forget_password_success'] = 'We have sent you an email, please check your email inbox or spam.';
	        header("Location: ".BASE_URL."forgetPassword");
	        exit();
        }
        else{
        	$conn->close();
	        header("Location: ".BASE_URL."forgetPassword");
	        exit();
        } 
    }
?>