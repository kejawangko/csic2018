<!-- main stylesheet -->
<link rel="stylesheet" href="<?php echo BASE_URL;?>/template/css/style.css">
<!-- responsive stylesheet -->
<link rel="stylesheet" href="<?php echo BASE_URL;?>/template/css/responsive.css">
<!-- SWAL -->
<link rel="stylesheet" href="<?php echo BASE_URL;?>/template/js/sweetalert/sweetalert.css">